//
//  AppDelegate.h
//  Defence
//
//  Created by huyingan on 1/2/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "GameCenterManager.h"
#import "AppSpecificValues.h"


@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate, GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate, GameCenterManagerDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
    
    GameCenterManager *gameCenterManager;
	int64_t  currentScore;
	NSString* currentLeaderBoard;	
	IBOutlet UILabel *currentScoreLabel;

}

- (void) initGameCenter ;
- (void) increaseScore:(long) nScore mode:(int) nMode;
- (void) reset;
- (void) showLeaderboard;
- (void) showAchievements;
- (void) reset;
- (void) submitScore;
- (void) checkAchievements;

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) GameCenterManager *gameCenterManager;
@property (nonatomic, assign) int64_t currentScore;
@property (nonatomic, retain) NSString* currentLeaderBoard;
@property (nonatomic, retain) UILabel *currentScoreLabel;

@end
