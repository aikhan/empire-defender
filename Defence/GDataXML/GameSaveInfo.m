//
//  GameSaveInfo.m
//  AREarth
//
//  Created by pirate on 11-8-16.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "GameSaveInfo.h"


@implementation GameSaveInfo

@synthesize m_nLevel;
@synthesize m_nScore;
@synthesize m_nMoney;
@synthesize m_nNewMoney;

- (id) init
{
    self = [super init];
    
    if (self)
    {
        m_nLevel = 0;
        m_nScore = 0;
        m_nMoney = 0;
        m_nNewMoney = 0;
    }
    
    
    return self;
} 

- (void) dealloc
{
    [super dealloc];
}

@end
