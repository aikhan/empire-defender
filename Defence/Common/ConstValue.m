//
//  ConstValue.c
//  Defence
//
//  Created by huyingan on 12/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "ConstValue.h"

CCTMXTiledMap *g_tmxMap = nil;
int nAryStartPoint[LEVEL_COUNT][DOOR_COUNT][2] = {
    
	{ {0, 0} },                                                             // level 1
	{ {13, 0} },                                                            // level 2
	{ {2, 0}, {35, 0} },                                                    // level 3
	{ {1, 0} },                                                             // level 4
	{ {0, 0}, {63, 0} },                                                    // level 5
	{ {5, 0}, {33, 0} },                                                    // level 6
	{ {2, 0}, {124, 0} },                                                   // level 7
	{ {51, 0}, {57, 0} },                                                   // level 8
	{ {8, 0}, {77, 0} },                                                    // level 9
	{ {0, 0} },                                                             // level 10
	{ {1, 0}, {46, 0}, {49, 0}, {53, 0}, {57, 0}, {60, 0} },                // level 11
	{ {15, 0} },                                                            // level 12
	{ {27, 0}, {27, 0}, {66, 0} },                                          // level 13
	{ {23, 0}, {33, 0} },                                                   // level 14
	{ {4, 0}, {6, 0}, {91, 0}, {94, 0} },                                   // level 15
	{ {4, 0}, {75, 0} },                                                    // level 16
	{ {4, 0}, {5, 0} },                                                     // level 17
	{ {2, 0}, {4, 0} },                                                     // level 18
	{ {2, 0}, {75, 0} },                                                    // level 19
	{ {26, 0}, {70, 0}, {73, 0} },                                          // level 20
	{ {3, 0}, {6, 0}, {7, 0} },                                             // level 21
	{ {2, 0}, {3, 0}, {46, 0} },                                            // level 22
	{ {53, 0}, {92, 0}, {94, 0} },                                          // level 23
	{ {31, 0}, {34, 0}, {73, 0}, {75, 0} },                                 // level 24
	{ {51, 0}, {57, 0} },                                                   // level 25
	{ {8, 0}, {77, 0} },                                                    // level 26
	{ {0, 0} },                                                             // level 27
	{ {1, 0}, {46, 0}, {49, 0}, {53, 0}, {57, 0}, {60, 0} },                // level 28
	{ {15, 0} },                                                            // level 29
	{ {27, 0}, {27, 0}, {66, 0} },                                          // level 30
	{ {8, 0}, {77, 0} },                                                    // level 31 - servival
};

int nAryStartDir[LEVEL_COUNT][DOOR_COUNT] = {
    { 1 },                          // level 1
    { 1 },                          // level 2
    { 1, 1 },                       // level 3
    { 1 },                          // level 4
    { 1, 1 },                       // level 5
    { 1, -1 },                      // level 6
    { 1, -1 },                      // level 7
    { -1, 1 },                      // level 8
    { 1, -1 },                      // level 9
    { 1 },                          // level 10
    { 1, -1, -1, 1, 1, 1 },         // level 11
    { 1 },                          // level 12
    { -1, 1, 1 },                   // level 13
    { -1, 1 },                      // level 14
    { 1, 1, -1, -1 },               // level 15
    { 1, -1 },                      // level 16
    { 1, 1 },                       // level 17
    { 1, 1 },                       // level 18
    { 1, -1 },                      // level 19
    { -1, -1, -1 },                 // level 20
    { 1, 1, 1 },                    // level 21
    { 1, 1, 1 },                    // level 22
    { -1, -1, -1 },                 // level 23
    { -1, 1, -1, -1 },              // level 24
    { -1, 1 },                      // level 25
    { 1, -1 },                      // level 26
    { 1 },                          // level 27
    { 1, -1, -1, 1, 1, 1 },         // level 28
    { 1 },                          // level 29
    { -1, 1, 1 },                   // level 30
    { 1, -1 },                      // level 31 - servival
};

int nAryLevelDoorCount[LEVEL_COUNT] = {1, 1, 2, 1, 2, 2, 2, 2, 2, 1, 
                                       6, 1, 3, 2, 4, 2, 2, 2, 2, 3, 
                                       3, 3, 3, 4, 2, 2, 1, 6, 1, 3,
                                       2};
int nAryLevelJewelPos[LEVEL_COUNT][2] = {
    {75, 22},   //  level 1
    {35, 58},   //  level 2
    {61, 69},   //  level 3
    {22, 58},   //  level 4
    {74, 38},   //  level 5
    {83, 55},   //  level 6
    {63, 38},   //  level 7
    { 7, 88},   //  level 8
    {47, 82},   //  level 9
    {17, 92},   //  level 10
    {101, 65},  //  level 11
    {17, 106},  //  level 12
    {48, 86},   //  level 13
    {29, 58},   //  level 14
    {48, 53},   //  level 15
    {40, 75},   //  level 16
    {90, 38},   //  level 17
    {67, 39},   //  level 18
    {39, 31},   //  level 19
    {35, 56},   //  level 20
    {71, 30},   //  level 21
    {93, 20},   //  level 22
    {30, 49},   //  level 23
    {58, 43},   //  level 24
    { 7, 88},   //  level 25
    {47, 82},   //  level 26
    {17, 92},   //  level 27
    {101, 65},  //  level 28
    {17, 106},  //  level 29
    {48, 86},   //  level 30
    {47, 82},   //  level 31 - servival
};

int nAryLevelMoney[LEVEL_COUNT] = {100, 100, 150, 150, 200, 200, 200, 250, 250, 250, 
                                   250, 250, 250, 250, 250, 300, 300, 300, 300, 300, 
                                   350, 350, 350, 350, 350, 400, 400, 400, 400, 400,
                                   100};

int nAryLevelTime[LEVEL_COUNT] = {60, 60, 60, 60, 60, 90, 90, 90, 90, 90, 
                                  80, 80, 90, 90, 90, 120, 120, 120, 120, 120, 
                                  150, 150, 150, 150, 150, 180, 180, 180, 180, 180,
                                  0};

int nAryLevelGold[LEVEL_COUNT] = {5, 5, 5, 10, 10, 10, 10, 10, 10, 10, 
                                  10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 
                                  10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                  10};

int nAryLevelMaxEnemy[LEVEL_COUNT] = {2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 
                                      4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 
                                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                                      6};

int nAryLevelMinEnemy[LEVEL_COUNT] = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 
                                      2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 
                                      4, 4, 4, 4, 5, 5, 5, 5, 5, 5,
                                      1};

int nAryEnemyCreatTime[LEVEL_COUNT] = {6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 
                                       5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 
                                       4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                                       4};

// ------------- fight information ----------- //

int nAryEnemyHealth[ENEMY_COUNT] = {14, 29, 45, 50, 75, 100};
int nAryEnemyAttack[ENEMY_COUNT] = {5, 6, 7, 8, 10, 13};
int nArySoldierHealth[SOLDIER_COUNT] = {20, 40, 60, 5, 6, 7, 8, 9};
int nArySoldierAttack[SOLDIER_COUNT] = {5, 6, 7, 15, 15, 17, 19, 21};

int nArySoldierGold[SOLDIER_COUNT] = {30, 45, 60, 30, 50, 80, 120, 200};
int nAryEnemyScore[ENEMY_COUNT] = {10, 15, 20, 25, 30, 35};

//CGFloat nAryEnemySpeed[ENEMY_COUNT] = {0.06f, 0.02f, 0.02f, 0.06f, 0.06f, 0.02f};
int nAryEnemySpeed[ENEMY_COUNT] = {4, 2, 2, 4, 4, 2};
int nAryArrowSpeed[SOLDIER_COUNT] = {0, 0, 0, 10, 10, 10, 15, 20};











