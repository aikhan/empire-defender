//
//  Fighter.h
//  Defence
//
//  Created by huyingan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entity.h"
#import "ConstValue.h"


@interface Soldier : Entity {
    
    int         m_nLevel;
    int         m_nType;
    CCSprite    *m_sCharacter;
    int         m_nSelEnemy;
    BOOL        m_bFlag;
    BOOL        m_bFightFlag;
    BOOL        m_bStop;
    BOOL        m_bDie;
    
    int         m_nHealth;
    int         m_nAttack;
}

-(int) isBlock: (CGPoint) tilePos;
-(CGPoint) tilePosFromLocation:(CGPoint) ptPos;
-(void) moveCharacter;
-(int) selectEnemy;
-(BOOL) checkBreak:(CGFloat)nFX firstY:(CGFloat)nFY secondX:(CGFloat)nSX secondY:(CGFloat)nSY;
-(void) cleanupCharater;


@property (readwrite) int m_nHealth;
@property (readwrite) int m_nAttack;
@property (readwrite) int m_nSelEnemy;
@property (readwrite) BOOL m_bFightFlag;
@property (readwrite) int m_nType;
@property (readwrite) BOOL m_bStop;
@property (readwrite) BOOL m_bDie;

@end
