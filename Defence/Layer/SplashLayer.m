//
//  MainMenuLayer.m
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SplashLayer.h"
#import "MainMenuLayer.h"
#import "ConstValue.h"

@implementation SplashLayer

static SplashLayer* g_cSplashLayer = nil;
//static int nCount = 0;
+(id) scene
{
	CCScene *scene = [CCScene node];
	SplashLayer *layer = [SplashLayer ShareInstance];
	[scene addChild: layer];
	return scene;
}

+(SplashLayer*) ShareInstance
{
    if (!g_cSplashLayer)
    {
        g_cSplashLayer = [[SplashLayer alloc] init];
    }
    return g_cSplashLayer;
}

-(id) init{

	if( (self = [super init]) ) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        backSprite = [CCSprite spriteWithFile:@"main.png"];
        backSprite.scale = DEVICE_SCALE;
       // backSprite.anchorPoint = ccp(0, 0);
        //DONOT Override default anchor point PLEASE!!!
        backSprite.position = ccp(size.width/2, size.height/2);
        [self addChild:backSprite];
        
        CCSprite *bandAidSprite = [CCSprite spriteWithFile:@"splashscreen.png"];
        bandAidSprite.scale = DEVICE_SCALE;
       // bandAidSprite.anchorPoint = ccp(0, 0);
        bandAidSprite.position = ccp(size.width/2, size.height/2);
        [self addChild:bandAidSprite];
        
        [self schedule: @selector(showMenuScreen:) interval:5];
    }
    return self;
}

-(void) showMenuScreen: (ccTime) delta
{
    [self unscheduleAllSelectors];
    [backSprite removeFromParentAndCleanup:YES];
    backSprite = nil;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[MainMenuLayer scene]]];
}

- (void) dealloc
{
	[super dealloc];
}


@end
