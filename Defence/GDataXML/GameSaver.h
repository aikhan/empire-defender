

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "GameSaveInfo.h"

@class CStageInfo;

@interface GameSaver : NSObject
{

}

+ (GameSaver*) sharedInstance;

+ (NSString *) getSaveFilePath : (NSString*) filename;

+ (BOOL) isExistSavedFile : (NSString*) filename;

- (void) removeSaveFile : (NSString*) filename;

- (void) readGameInfo:(GameSaveInfo *) savedinfo;

- (void) writeGameInfo: (GameSaveInfo *) gameinfo;


@end
