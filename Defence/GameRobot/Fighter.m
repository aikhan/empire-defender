//
//  Fighter.m
//  Defence
//
//  Created by huyingan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Fighter.h"
#import "GameViewLayer.h"
#import "MainMenuLayer.h"

@implementation Fighter

@synthesize m_nOrder;
@synthesize m_bSeat;
@synthesize m_nDir;
@synthesize m_nAction;

-(id) init:(int)nType order:(int)nOrder
{
	if( (self=[super init] )) {

        //m_nLevel = [GameViewLayer ShareInstance].m_nLevel;
        m_nType = nType;    // solider type : 0 ~ 2 (fighter)
        [self attachImage];
        m_bFightFlag = NO;
        m_bFlag = NO;
        m_bStop = NO;
        m_bSeat = NO;
        m_nOrder = nOrder;
        m_spNoMove = nil;
        m_nDir = 1;
        m_nCheck = ACTION_NOTHING; // check running action
        m_nDieCount = 100;
        m_bDie = NO;
        
        m_nHealth = nArySoldierHealth[nType];
        m_nAttack = nArySoldierAttack[nType];
        
        [self attachNoMove];
        [self attachSign];

	}
	return self;
}

-(void) attachSign
{
    m_spSign = [CCSprite spriteWithFile:[NSString stringWithFormat:@"tooltip_bg%d.png", m_nType + 1]];
    m_spSign.position = ccp(120, 100);
    [self addChild:m_spSign z:LAYER_ENTITY];
}

-(void) removeSign
{
    if (m_spSign)
    {
        [self removeChild:m_spSign cleanup:YES];
    }
    m_spSign = nil;
}


-(void) attachNoMove
{
    if (!m_spNoMove)
    {
        m_spNoMove = [CCSprite spriteWithFile:@"no_move.png"];
        m_spNoMove.position = ccp(35, 30);
        [self addChild:m_spNoMove z:LAYER_ENTITY];
    }
}

-(void) removeNoMove
{
    if (m_spNoMove)
    {
        [self removeChild:m_spNoMove cleanup:YES];
    }
    m_spNoMove = nil;
}

-(void) attachImage
{
    [self setAnimation:ACTION_READY repeat:YES];
}

-(void) startFight
{
    [self setAnimation:ACTION_READY repeat:NO];
    [self schedule: @selector(fighting:) interval:ACTION_SPEED];

    m_nAction = ACTION_FIGHT1;
}

-(void) fighting: (ccTime) delta
{
    [self setAnimation:m_nAction repeat:NO];

    m_nAction ++;
    if (m_nAction == ACTION_READY)
        m_nAction = ACTION_FIGHT1;
}

-(void) dieFighter
{
    /*
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryEnemy count]; i ++)
    {
        int nIndex = [[GameViewLayer ShareInstance].m_aryFighter indexOfObject:self];
        Enemy *enemy = [[GameViewLayer ShareInstance].m_aryEnemy objectAtIndex:i];
        if (enemy.m_nFighter > nIndex)
        {
            enemy.m_nFighter --;
        }
    }
     
    [[GameViewLayer ShareInstance].m_aryFighter removeObject:self];
    */
    
    m_nAction = ACTION_DIE;
    [self setAnimation:ACTION_DIE repeat:NO];
    [self schedule: @selector(removeFighter:) interval:0.1];
}

-(void) removeFighter: (ccTime) delta
{
    m_nDieCount -= 5;
    [self setOpacity:m_nDieCount];
    if (m_nDieCount < 0)
    {
        [self unscheduleAllSelectors];
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
        [self cleanupCharater];
    }
}

-(void) setAnimation:(int)nAction repeat:(BOOL)bForever
{
    if (m_nCheck == nAction) // if action is same , return
        return;
    
    [self stopAllActions];

    m_nCheck = nAction;
    int nStep = 0;
    int nStartAct = 0;
    
    switch (m_nCheck) 
    {
        case ACTION_DIE:
            nStep = 6;
            nStartAct = 0;
            break;
        case ACTION_FIGHT1:
            nStep = 12;
            nStartAct = 1;
            break;
        case ACTION_FIGHT2:
            nStep = 12;
            nStartAct = 2;
            break;
        case ACTION_FIGHT3:
            nStep = 12;
            nStartAct = 3;
            break;
        case ACTION_FIGHT4:
            nStep = 12;
            nStartAct = 4;
            break;
        case ACTION_FIGHT5:
            nStep = 12;
            nStartAct = 5;
            break;
        case ACTION_FIGHT6:
            nStep = 12;
            nStartAct = 6;
            break;
        case ACTION_READY:
            nStep = 1;
            nStartAct = 1;
            break;
            
        default:
            break;
    }

    //CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier%d_act.png", m_nType + 1]];
    if (bForever)
    {
        CCAnimation *animation = [CCAnimation animation];
        for(int i = 0; i < nStep; i++) {
            [animation addFrameWithTexture:[[MainMenuLayer ShareInstance] getTexture:0 order:m_nType + 1] rect:CGRectMake(i * 80, nStartAct * 80 + 2, 80, 80)];
        }
        CCAnimate *animateAction = [CCAnimate actionWithDuration: ACTION_SPEED animation: animation restoreOriginalFrame: NO];
        CCRepeatForever *repeatAction = [CCRepeatForever actionWithAction:animateAction];
        [self runAction: repeatAction];
    }
    else
    {
        CCAnimation *animation = [[CCAnimation alloc] initWithName:@"idle" delay:0.05];
        for(int i = 0; i < nStep; i++) {
            [animation addFrameWithTexture:[[MainMenuLayer ShareInstance] getTexture:0 order:m_nType + 1] rect:CGRectMake(i * 80, nStartAct * 80 + 2, 80, 80)];
        }
        CCAnimate   *actionEffect = [CCAnimate actionWithAnimation:animation restoreOriginalFrame: NO];
        CCRepeat    *repeatAction = [CCRepeat actionWithAction:actionEffect times:1];
        [self runAction: repeatAction];
    }
}

-(void) startAnimaion
{
    [self schedule: @selector(update:) interval:0.01];
}

-(void) update: (ccTime) delta
{
    [self moveCharacter];
    
    if (m_bStop == YES)
    {
        [self unscheduleAllSelectors];
        return;
    }
}

-(void) cleanupCharater {
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
}

-(void) dealloc
{
	[super dealloc];
}

@end
