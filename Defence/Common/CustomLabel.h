//
//  CustomLabel.h
//  AREarth
//
//  Created by pirate on 11-8-2.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CustomLabel : UIImage {

    UIImage *m_FontImg;
    CGFloat m_fWidth;
    CGFloat m_fHeight;
    int m_nSepaNumber;
    
}

- (id)init:(CGFloat)width img_height:(CGFloat)height sepa_width:(int)sepa_number;
- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect;
- (UIImage*)getPara_CustomFont:(UIImage *)imageToCrop nOrder:(int)order;
- (UIImage*)getCustomImage:(UIImage *)imageToCrop nOrder:(int)order;

@property (nonatomic, retain) UIImage *m_FontImg;
@end
