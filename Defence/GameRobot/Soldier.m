//
//  Fighter.m
//  Defence
//
//  Created by huyingan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Soldier.h"
#import "GameViewLayer.h"

@implementation Soldier

@synthesize m_nAttack;
@synthesize m_nHealth;
@synthesize m_nSelEnemy;
@synthesize m_nType;
@synthesize m_bStop;
@synthesize m_bDie;
@synthesize m_bFightFlag;

-(int) selectEnemy
{
    if (m_bFlag == YES) return m_nSelEnemy;

    if ([[GameViewLayer ShareInstance].m_aryEnemy count] == 0) return -1;

    int nIndex = 0;
    int nAryEnemy[[[GameViewLayer ShareInstance].m_aryEnemy count]];
    CGFloat nAryGapX[[[GameViewLayer ShareInstance].m_aryEnemy count]];
    
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryEnemy count]; i ++)
    {
        nAryEnemy[i] = 0;
        nAryGapX[i] = 0;
    }

    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryEnemy count]; i ++)
    {
        Enemy *enemy = [[GameViewLayer ShareInstance].m_aryEnemy objectAtIndex:i];
        
        if (enemy.m_bDie == YES)
            continue;
        
        CGFloat nEnemy_X = enemy.position.x;
        CGFloat nEnemy_Y = enemy.position.y + OFFSET - 20;
        CGFloat nSelf_X = self.position.x;
        CGFloat nSelf_Y = self.position.y + OFFSET - 20;
        
        BOOL bCheck = [self checkBreak:nEnemy_X firstY:nEnemy_Y secondX:nSelf_X secondY:nSelf_Y];
        
        if (bCheck == NO)
        {
            nAryEnemy[nIndex] = i;
            nAryGapX[nIndex] = ABS(nEnemy_X - nSelf_X);
            nIndex ++;
        }
    }
    
    if (nIndex == 0)
        return -1;
    
    CGFloat nMinX = nAryGapX[0];
    int nMinEnemy = nAryEnemy[0];
    for (int i = 1; i < nIndex; i ++)
    {
        if (nMinX > nAryGapX[i])
        {
            nMinEnemy = nAryEnemy[i];
            nMinX = nAryGapX[i];
        }
    }

    m_bFlag = YES;
    return nMinEnemy;
}

// if there is obstacle between enemy and soldier, return YES, if not, return no.
-(BOOL) checkBreak:(CGFloat)nFX firstY:(CGFloat)nFY secondX:(CGFloat)nSX secondY:(CGFloat)nSY
{
    if (nFX < nSX)
    {
        CGFloat fTempX = nFX;
        CGFloat fTempY = nFY;
        while (fTempX < nSX) {
            fTempX += (nSX - nFX) / 100;
            fTempY += (nSY - nFY) / 100;
            CGPoint ptPos = [self tilePosFromLocation:ccp(fTempX, fTempY)];
            int result = [self isBlock:ptPos];
            if (result > 0)
            {
                if (ABS(fTempX - nSX) < OFFSET && ABS(fTempY - nSY) < OFFSET)
                    continue;
                return YES;
            }
        }
    }
    else
    {
        CGFloat fTempX = nSX;
        CGFloat fTempY = nSY;
        while (fTempX < nFX) {
            fTempX += (nFX - nSX) / 100;
            fTempY += (nFY - nSY) / 100;
            CGPoint ptPos = [self tilePosFromLocation:ccp(fTempX, fTempY)];
            int result = [self isBlock:ptPos];
            if (result > 0)
            {
                if (ABS(fTempX - nSX) < OFFSET && ABS(fTempY - nSY) < OFFSET)
                    continue;
                return YES;
            }
        }
    }
    return NO;
}


-(void) moveCharacter
{
    if (m_bStop)
        return;
    
    CGPoint pos = ccp(0, 0);
    pos.x = self.position.x;
    pos.y = self.position.y;
    
    CGPoint ptPos = [self tilePosFromLocation:ccp(pos.x, pos.y - OFFSET)];
    int result = [self isBlock:ptPos];
    
    if (result == DIR_NOTHING) // nothing block
    {
        pos.y -= 2;
    }
    else
    {
        m_bStop = YES;
    }
    self.position = ccp(pos.x, pos.y);
}

-(int) isBlock: (CGPoint) tilePos
{
	int nIsBlock = 0;
    if(tilePos.x < g_tmxMap.mapSize.width && tilePos.x >= 0 && tilePos.y >= 0 && tilePos.y < g_tmxMap.mapSize.height)
    {
        int tileGID = [[g_tmxMap layerNamed:@"Stair_Layer"] tileGIDAt:tilePos];
        if(tileGID != 0)
        {
            NSDictionary *properties = [g_tmxMap propertiesForGID:tileGID];
            if(properties)
            {
                NSString *isBlock = [properties valueForKey:@"Stair"];
                nIsBlock = [isBlock intValue];
                isBlock = nil;
            }
        }
    }
	return nIsBlock;
}

-(CGPoint) tilePosFromLocation:(CGPoint) ptPos
{
	ptPos.x = (int)(ptPos.x / TILE_WIDTH);
	ptPos.y = (int)(ptPos.y / TILE_HEIGHT);
    
    ptPos.y = g_tmxMap.mapSize.height - ptPos.y - 1;
	return ptPos;
}


-(void) cleanupCharater
{
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [self cleanupCharater];
	[super dealloc];
}

@end
