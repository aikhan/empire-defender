//
//  ConstValue.h
//  Defence
//
//  Created by huyingan on 12/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "cocos2d.h"

// --------------- Main Menu ---------------- //

#define MENU_POS_X          320
#define MENU_POS_Y          250


// --------------- Game Screen ---------------- //

#define DEVICE_WIDTH        480
#define DEVICE_HEIGHT       320
#define DEVICE_SCALE        0.5f
#define TILE_WIDTH          16
#define TILE_HEIGHT         8
#define MAP_START_X         10.0f
#define MAP_START_Y         76.0f
#define DOOR_COUNT          6
#define LAYER_MAP           10
#define LAYER_GAME_BACK     1
#define LAYER_GAME_BACK_SKY 0
#define LAYER_ENTITY        2
#define LAYER_MESSAGE       100
#define LAYER_ENVIRONMENT   11

#define TEXT_LEVEL_X        423
#define TEXT_LEVEL_Y        22
#define TEXT_MONEY_X        333
#define TEXT_MONEY_Y        22
#define TEXT_GOLD_X         383
#define TEXT_GOLD_Y         22
#define TEXT_TIME_X         270
#define TEXT_TIME_Y         305
#define TEXT_SCORE_X        327
#define TEXT_SCORE_Y        305

#define OFFSET              40

// ----------- MAP Direction ------------ //
#define DIR_NOTHING         0   // there is no any block
#define DIR_UP              1   // block is up direction
#define DIR_DOWN            2   // block is down direction
#define DIR_STRAIGHT        3   // block is straight direction
#define DIR_GOLD            5   // block is gold position

#define DIR_LEFT_TO_RIGHT   6   // enemy is going to go with up direction, if enemy goes from left to right, go up
#define DIR_RIGHT_TO_LEFT   7   // enemy is going to go with up direction, if enemy goes from right to left, go up
#define DIR_END_LR          8   // if enemy goes from left to right, change direction
#define DIR_END_RL          9   // if enemy goes from right to left, change direction

#define DIR_LTR_SEAT        26  // it's same with DIR_LEFT_TO_RIGHT, but solider can't stand on this block
#define DIR_RTL_SEAT        27  // it's same with DIR_RIGHT_TO_LEFT, but solider can't stand on this block

#define DIR_UP_SEAT         11  // block is seat that soldier can't stand on up blocks
#define DIR_DOWN_SEAT       12  // block is seat that soldier can't stand on down blocks
#define DIR_STR_SEAT        13  // block is seat that soldier can't stand on straight blocks

#define DIR_LR_STR          14  // if enemy goes from left to right, change direction, if right to left, no change
#define DIR_RL_STR          18  // if enemy goes from right to left, change direction, if left to right, no change
#define DIR_LR_RAND         15  // if enemy goes from left to right, some enemy will go straight, some will change direction
#define DIR_RL_RAND         16  // if enemy goes from right to left, some enemy will go straight, some will change direction
#define DIR_STR_RAND        17  // enemy will define random direction


// ----------- Eenmy speed ----------- //
#define ENEMY1_SPEED_X      4
#define ENEMY1_SPEED_Y      2
#define ACTION_SPEED        1


#define LEVEL_COUNT         31

// ----------- Define Action ------------ //
#define ACTION_STR              0
#define ACTION_UP               1
#define ACTION_DOWN             2
#define ACTION_DIE              3
#define ACTION_FIGHT1           4
#define ACTION_FIGHT2           5
#define ACTION_FIGHT3           6
#define ACTION_FIGHT4           7
#define ACTION_FIGHT5           8
#define ACTION_FIGHT6           9
#define ACTION_READY            10
#define ACTION_NOTHING          100

// ----------- Health of robots --------- //


#define ARCHER_HEALTH           5

#define OFFSET_TOUCH            50
#define MAX_ARROW               20

#define OFFSET_OCCUPY_X         60
#define OFFSET_OCCUPY_Y         40

#define ENEMY_COUNT             6
#define SOLDIER_COUNT           8
#define ENEMY_CREATE_TIME       6

extern CCTMXTiledMap *g_tmxMap;
extern int nAryStartPoint[LEVEL_COUNT][DOOR_COUNT][2];
extern int nAryStartDir[LEVEL_COUNT][DOOR_COUNT];
extern int nAryEnemyHealth[ENEMY_COUNT];
extern int nAryEnemyAttack[ENEMY_COUNT];
extern int nAryEnemyScore[ENEMY_COUNT];
extern int nArySoldierHealth[SOLDIER_COUNT];
extern int nArySoldierAttack[SOLDIER_COUNT];
extern int nAryLevelDoorCount[LEVEL_COUNT];
extern int nAryLevelJewelPos[LEVEL_COUNT][2];
extern int nAryLevelGold[LEVEL_COUNT];

extern int nArySoldierGold[SOLDIER_COUNT];
extern int nAryLevelMaxEnemy[LEVEL_COUNT];
extern int nAryLevelMinEnemy[LEVEL_COUNT];
//extern CGFloat nAryEnemySpeed[ENEMY_COUNT];
extern int nAryEnemySpeed[ENEMY_COUNT];
extern int nAryArrowSpeed[SOLDIER_COUNT];
extern int nAryEnemyCreatTime[LEVEL_COUNT];

extern int nAryLevelMoney[LEVEL_COUNT];
extern int nAryLevelTime[LEVEL_COUNT];








