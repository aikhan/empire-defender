//
//  Fighter.h
//  Defence
//
//  Created by huyingan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Soldier.h"
#import "ConstValue.h"


@interface Fighter : Soldier 
{
    
    int         m_nCheck;
    int         m_nAction;
    int         m_nOrder;   // order index of array
    int         m_nDir;     // -1 : left to right, 1 : right to left
    BOOL        m_bSeat;
    CCSprite    *m_spNoMove;
    int         m_nDieCount;
    CCSprite    *m_spSign;

}

-(id) init:(int)nType order:(int)nOrder;
-(void) startAnimaion;
-(void) startFight;
-(void) setAnimation:(int)nAction repeat:(BOOL)bForever;
-(void) attachImage;
-(void) attachNoMove;
-(void) attachSign;
-(void) removeNoMove;
-(void) removeSign;
-(void) dieFighter;
-(void) cleanupCharater;

@property (readwrite) int m_nOrder;
@property (readwrite) BOOL m_bSeat;
@property (readwrite) int m_nDir;
@property (readwrite) int m_nAction;

@end
