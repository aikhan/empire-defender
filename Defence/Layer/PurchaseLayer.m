//
//  MainMenuLayer.m
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PurchaseLayer.h"
#import "MainMenuLayer.h"
#import "ConstValue.h"


@implementation PurchaseLayer

static PurchaseLayer* g_cPurchaseLayer = nil;

+(id) scene
{
	CCScene *scene = [CCScene node];
	PurchaseLayer *layer = [PurchaseLayer ShareInstance];
	[scene addChild: layer];
	return scene;
}

+(PurchaseLayer*) ShareInstance
{
    if (!g_cPurchaseLayer)
    {
        g_cPurchaseLayer = [[PurchaseLayer alloc] init];
    }
    return g_cPurchaseLayer;
}

-(id) init
{
	if( (self = [super init]) ) 
    {
        
        backSprite = [CCSprite spriteWithFile:@"menu.png"];
        backSprite.scale = DEVICE_SCALE;
        backSprite.anchorPoint = ccp(0, 0);
        [self addChild:backSprite z:LAYER_GAME_BACK];

        [self attachMenu];
        
        m_bDone = NO;
        
        if ([self canPayment] == YES)
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        else
        {
            UIAlertView* dialog = [[UIAlertView alloc] init];
            [dialog setDelegate:self];
            [dialog setTitle:@"In App Purchase"];
            [dialog setMessage:@"You can't call In-App-Purchase Service on this device."];
            [dialog addButtonWithTitle:@"OK"];
            [dialog show];
        }
    }
    return self;
}

-(BOOL) canPayment
{
    if ([SKPaymentQueue canMakePayments])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void) requestProductData
{
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObjects: @"com.alankong.defense.cash1", @"com.alankong.defense.cash2", @"com.alankong.defense.cash3", nil ]];
    request.delegate = self;
    [request start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	for (unsigned int i = 0 ; i < [response.products count]; i++) 
    {
		SKProduct* product = [response.products objectAtIndex:i];
		NSNumberFormatter *mm = [[NSNumberFormatter alloc] init];
		[mm setFormatterBehavior:NSNumberFormatterBehavior10_4];
		[mm setNumberStyle:NSNumberFormatterCurrencyStyle];
		[mm setLocale:product.priceLocale];
        
        /*
		NSString *strPrice = [mm stringFromNumber:product.price];
		NSString *strID = product.productIdentifier;
		NSString *strDesc = product.localizedDescription;
        
        NSLog(@"%@, %@, %@", strPrice, strID, strDesc);
         */
    }
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
	SKPayment* payment = transaction.payment;
	NSString *result = payment.productIdentifier;
    
    // -------- plus money ------- //
    GameSaver *gameSaver = [[GameSaver alloc] init];
    GameSaveInfo *gameInfo = [[GameSaveInfo alloc] init];
    [gameSaver readGameInfo:gameInfo];
    
    int nNewMoney = gameInfo.m_nNewMoney;
    
    if (result == @"com.alankong.defense.cash1")
    {
        nNewMoney += 1000;
    }

    if (result == @"com.alankong.defense.cash2")
    {
        nNewMoney += 5000;
    }

    if (result == @"com.alankong.defense.cash3")
    {
        nNewMoney += 10000;
    }
    gameInfo.m_nNewMoney = nNewMoney;
    
    [gameSaver writeGameInfo:gameInfo];
    [gameInfo release];
    [gameSaver release];
    // --------------------------- //
    

    UIAlertView* dialog = [[UIAlertView alloc] init];
	[dialog setDelegate:self];
	[dialog setTitle:@"In App Purchase"];
	[dialog setMessage:@"Purchase Success!"];
	[dialog addButtonWithTitle:@"OK"];
	[dialog show];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
	if (transaction.error.code != SKErrorPaymentCancelled)
	{
		// Optionally, display an error here.
	}
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}


- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
    m_bDone = NO;
}

-(void) attachMenu
{
    item_back = [CCMenuItemImage itemFromNormalImage:@"btn_back.PNG" selectedImage:@"btn_back_down.PNG" target:self selector:@selector(clickBack:)];
    item_money1 = [CCMenuItemImage itemFromNormalImage:@"coin1.png" selectedImage:@"coin1-1.png" target:self selector:@selector(clickMoney1:)];
    item_money2 = [CCMenuItemImage itemFromNormalImage:@"coin2.png" selectedImage:@"coin2-1.png" target:self selector:@selector(clickMoney2:)];
    item_money3 = [CCMenuItemImage itemFromNormalImage:@"coin3.png" selectedImage:@"coin3-1.png" target:self selector:@selector(clickMoney3:)];


    mainMenu = [CCMenu menuWithItems:item_money1, item_money2, item_money3, item_back, nil];
    mainMenu.position = CGPointZero;
    
    item_back.scale = DEVICE_SCALE;
    item_money1.scale = DEVICE_SCALE;
    item_money2.scale = DEVICE_SCALE;
    item_money3.scale = DEVICE_SCALE;
    
    item_money1.position = ccp(MENU_POS_X, MENU_POS_Y - 30);
    item_money2.position = ccp(MENU_POS_X, MENU_POS_Y - 60);
    item_money3.position = ccp(MENU_POS_X, MENU_POS_Y - 90);
    item_back.position = ccp(MENU_POS_X, MENU_POS_Y - 160);
    
    [self addChild: mainMenu z:LAYER_ENVIRONMENT];
}

-(void) clickBack: (id) sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[MainMenuLayer scene]]];
}

-(void) clickMoney1: (id) sender
{
    if (m_bDone == YES)
        return;
    m_bDone = YES;
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:@"com.alankong.defense.cash1"];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void) clickMoney2: (id) sender
{
    if (m_bDone == YES)
        return;
    m_bDone = YES;
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:@"com.alankong.defense.cash2"];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void) clickMoney3: (id) sender
{
    if (m_bDone == YES)
        return;
    m_bDone = YES;
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:@"com.alankong.defense.cash3"];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void) dealloc
{
	[super dealloc];
}

@end
