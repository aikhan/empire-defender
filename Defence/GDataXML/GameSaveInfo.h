//
//  GameSaveInfo.h
//  AREarth
//
//  Created by pirate on 11-8-16.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GameSaveInfo : NSObject {

    int         m_nMoney;
    int         m_nScore;
    int         m_nLevel;
    int         m_nNewMoney;
}

@property (readwrite)         int       m_nLevel;
@property (readwrite)         int       m_nMoney;
@property (readwrite)         int       m_nScore;
@property (readwrite)         int       m_nNewMoney;


@end
