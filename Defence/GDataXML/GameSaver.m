
#import "GameSaver.h"
#import "GDataXMLNode.h"


@implementation GameSaver

static GameSaver* g_GameSaver = nil;

+ (GameSaver*) sharedInstance
{
	if(!g_GameSaver)
	{		
		g_GameSaver = [[GameSaver alloc] init];
	}
	
	return g_GameSaver;	
}

- (id) init
{
	if ( (self = [super init]) )
	{
	}
	
	return self;
}

- (void) dealloc
{
	[super dealloc];
}

+ (NSString *) getSaveFilePath : (NSString*) filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    
	return [documentsDirectory stringByAppendingPathComponent: filename];
}

+ (BOOL) isExistSavedFile : (NSString*) filename
{	
    if( [[NSFileManager defaultManager] fileExistsAtPath: [GameSaver getSaveFilePath: filename]] )
	{
        return YES;
    }
	
	return NO;
}

- (void) removeSaveFile : (NSString*) filename
{
	if( [GameSaver isExistSavedFile: filename] )
	{
		NSError* errorMsg = nil;
		
		if( [[NSFileManager defaultManager] removeItemAtPath: [GameSaver getSaveFilePath: filename] error: &errorMsg] )
		{
			return;
		}
		
	}
}


- (void) writeGameInfo:(GameSaveInfo *)gameinfo
{
    GDataXMLElement * rootElement = [GDataXMLNode elementWithName:@"GameInfo"];

    [rootElement addChild: [GDataXMLNode elementWithName:[NSString stringWithFormat:@"level"]
                                             stringValue:[NSString stringWithFormat:@"%i", gameinfo.m_nLevel]]];
    
    [rootElement addChild: [GDataXMLNode elementWithName:[NSString stringWithFormat:@"score"]
                                             stringValue:[NSString stringWithFormat:@"%i", gameinfo.m_nScore]]];
    
    [rootElement addChild: [GDataXMLNode elementWithName:[NSString stringWithFormat:@"money"]
                                             stringValue:[NSString stringWithFormat:@"%i", gameinfo.m_nMoney]]];

    [rootElement addChild: [GDataXMLNode elementWithName:[NSString stringWithFormat:@"new_money"]
                                             stringValue:[NSString stringWithFormat:@"%i", gameinfo.m_nNewMoney]]];
    
    
    GDataXMLDocument *document = [[GDataXMLDocument alloc] initWithRootElement: rootElement];
    
    if ( nil == document )
        return;
    
    [document setCharacterEncoding: @"UTF-8"];
    
    [[document XMLData] writeToFile: [GameSaver getSaveFilePath: @"game_info.xml"]  atomically:YES];
    
    [document release];
    
}
- (void) readGameInfo:(GameSaveInfo *)savedinfo
{
	if( NO == [GameSaver isExistSavedFile: @"game_info.xml"] )
		return;
	
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile: [GameSaver getSaveFilePath: @"game_info.xml"]];
    NSError *error = nil;
	
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    if( doc == nil )
		return;
    
    NSArray *elementList = [doc nodesForXPath:@"GameInfo" error:nil];
    
	for (GDataXMLElement *element in elementList)
	{
        NSArray *arr;
        
        arr = [element elementsForName:[NSString stringWithFormat:@"level"]];
        if( [arr count] > 0 )
        {
            GDataXMLElement *nLevel = (GDataXMLElement *)[arr objectAtIndex:0];
            savedinfo.m_nLevel = [[nLevel stringValue] intValue];
        }
        arr = [element elementsForName:[NSString stringWithFormat:@"score"]];
        if( [arr count] > 0 )
        {
            GDataXMLElement *nScore = (GDataXMLElement *)[arr objectAtIndex:0];
            savedinfo.m_nScore = [[nScore stringValue] intValue];
        }
        arr = [element elementsForName:[NSString stringWithFormat:@"money"]];
        if( [arr count] > 0 )
        {
            GDataXMLElement *nLaser = (GDataXMLElement *)[arr objectAtIndex:0];
            savedinfo.m_nMoney = [[nLaser stringValue] intValue];
        }
        arr = [element elementsForName:[NSString stringWithFormat:@"new_money"]];
        if( [arr count] > 0 )
        {
            GDataXMLElement *nLaser = (GDataXMLElement *)[arr objectAtIndex:0];
            savedinfo.m_nNewMoney = [[nLaser stringValue] intValue];
        }
        
	}
    
    [doc release];
    [xmlData release];
    
}

@end
