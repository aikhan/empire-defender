//
//  MainMenuLayer.m
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SettingViewLayer.h"
#import "MainMenuLayer.h"
#import "ConstValue.h"


@implementation SettingViewLayer

@synthesize m_bBackMusic;
@synthesize m_bEffectMusic;

static SettingViewLayer* g_cSettingLayer = nil;

+(id) scene
{
	CCScene *scene = [CCScene node];
	SettingViewLayer *layer = [SettingViewLayer ShareInstance];
	[scene addChild: layer];
	return scene;
}

+(SettingViewLayer*) ShareInstance
{
    if (!g_cSettingLayer)
    {
        g_cSettingLayer = [[SettingViewLayer alloc] init];
    }
    return g_cSettingLayer;
}

-(id) init
{
	if( (self = [super init]) ) {
        
        backSprite = [CCSprite spriteWithFile:@"menu.png"];
        backSprite.scale = DEVICE_SCALE;
        backSprite.anchorPoint = ccp(0, 0);
        [self addChild:backSprite z:LAYER_GAME_BACK];

        backMusic = [CCSprite spriteWithFile:@"background_music.png"];
        backMusic.scale = DEVICE_SCALE;
        backMusic.position = ccp(MENU_POS_X + 20, MENU_POS_Y - 30);
        [self addChild:backMusic z:LAYER_ENVIRONMENT];

        effectMusic = [CCSprite spriteWithFile:@"effect_music.png"];
        effectMusic.scale = DEVICE_SCALE;
        effectMusic.position = ccp(MENU_POS_X, MENU_POS_Y - 60);
        [self addChild:effectMusic z:LAYER_ENVIRONMENT];

        [self attachMenu];
        
        m_bBackMusic = YES;
        m_bEffectMusic = YES;
    }
    return self;
}

-(void) attachMenu
{
    item_back = [CCMenuItemImage itemFromNormalImage:@"btn_back.PNG" selectedImage:@"btn_back_down.PNG" target:self selector:@selector(clickBack:)];
    item_bg_on = [CCMenuItemImage itemFromNormalImage:@"chk2.PNG" selectedImage:@"chk2.PNG" target:self selector:@selector(clickBackMusic:)];
    item_bg_off = [CCMenuItemImage itemFromNormalImage:@"chk1.PNG" selectedImage:@"chk1.PNG" target:self selector:@selector(clickBackMusic:)];
    item_effect_on = [CCMenuItemImage itemFromNormalImage:@"chk2.PNG" selectedImage:@"chk2.PNG" target:self selector:@selector(clickEffectMusic:)];
    item_effect_off = [CCMenuItemImage itemFromNormalImage:@"chk1.PNG" selectedImage:@"chk1.PNG" target:self selector:@selector(clickEffectMusic:)];
    
    mainMenu = [CCMenu menuWithItems:item_bg_on, item_bg_off, item_effect_on, item_effect_off, item_back, nil];
    mainMenu.position = CGPointZero;
    
    item_back.scale = DEVICE_SCALE;
    item_bg_on.scale = DEVICE_SCALE;
    item_effect_on.scale = DEVICE_SCALE;
    item_bg_off.scale = DEVICE_SCALE;
    item_effect_off.scale = DEVICE_SCALE;
    
    item_bg_on.position = ccp(MENU_POS_X - 75, MENU_POS_Y - 30);
    item_effect_on.position = ccp(MENU_POS_X - 75, MENU_POS_Y - 60);
    item_bg_off.position = ccp(MENU_POS_X - 75, MENU_POS_Y - 30);
    item_effect_off.position = ccp(MENU_POS_X - 75, MENU_POS_Y - 60);
    item_back.position = ccp(MENU_POS_X, MENU_POS_Y - 160);
    
    item_bg_off.visible = NO;
    item_effect_off.visible = NO;
    [self addChild: mainMenu z:LAYER_ENVIRONMENT];
}

-(void) clickBackMusic: (id) sender
{
    if (m_bBackMusic == YES)
    {
        m_bBackMusic = NO;
        item_bg_off.visible = YES;
        item_bg_on.visible = NO;
        [[MainMenuLayer ShareInstance] pauseBackSound];
    }
    else
    {
        m_bBackMusic = YES;
        item_bg_off.visible = NO;
        item_bg_on.visible = YES;
        [[MainMenuLayer ShareInstance] resumeBackSound];
    }
}

-(void) refreshCheckbox
{
    
    if (m_bBackMusic == YES)
    {
        item_bg_off.visible = NO;
        item_bg_on.visible = YES;
    }
    else
    {
        item_bg_off.visible = YES;
        item_bg_on.visible = NO;
    }
}

-(void) clickEffectMusic: (id) sender
{
    if (m_bEffectMusic == YES)
    {
        m_bEffectMusic = NO;
        item_effect_off.visible = YES;
        item_effect_on.visible = NO;
    }
    else
    {
        m_bEffectMusic = YES;
        item_effect_off.visible = NO;
        item_effect_on.visible = YES;
    }
}

-(void) clickBack: (id) sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[MainMenuLayer scene]]];
}


- (void) dealloc
{
	[super dealloc];
}

@end
