//
//  MainMenuLayer.h
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <StoreKit/StoreKit.h>
#import "GameSaveInfo.h"
#import "GameSaver.h"

@interface PurchaseLayer : CCLayer <SKProductsRequestDelegate, SKPaymentTransactionObserver>
{

    CCSprite        *backSprite;

    CCMenu          *mainMenu;
    CCMenuItem      *item_money1;
    CCMenuItem      *item_money2;
    CCMenuItem      *item_money3;
    CCMenuItem      *item_back;
    
    BOOL            m_bDone;
}

+(id) scene;
+(PurchaseLayer*) ShareInstance;

-(void) attachMenu;
-(void) clickBack: (id) sender;
-(void) clickMoney1: (id) sender;
-(void) clickMoney2: (id) sender;
-(void) clickMoney3: (id) sender;

-(BOOL) canPayment;
-(void) requestProductData;
-(void) completeTransaction: (SKPaymentTransaction *)transaction;
-(void) restoreTransaction: (SKPaymentTransaction *)transaction;
-(void) failedTransaction: (SKPaymentTransaction *)transaction;

@end
