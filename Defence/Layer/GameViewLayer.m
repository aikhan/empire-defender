//
//  GameViewLayer.m
//  Defence
//
//  Created by huyingan on 12/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameViewLayer.h"
#import "MainMenuLayer.h"
#import "SettingViewLayer.h"
#import "ConstValue.h"
#import "GameSaveInfo.h"
#import "GameSaver.h"
#import "SettingsManager.h"
#import "SNAdsManager.h"

@implementation GameViewLayer

@synthesize m_nLevel;
@synthesize m_archer;
@synthesize m_fighter;
@synthesize m_aryEnemy;
@synthesize m_aryArcher;
@synthesize m_aryFighter;
@synthesize m_nGold;
@synthesize m_nMoney;
@synthesize m_aryArrow;
@synthesize m_bCreate;

static GameViewLayer* g_cGameViewLayer = nil;
static int g_nLevel = 1;
static int g_nRemainMoney = 0;
static int g_nScore = 0;

static int g_nAddFlag1 = 0;
static int g_nAddFlag2 = 0;
static int g_nAddFlag3 = 0;


+(id) scene
{
    CCScene *scene = [CCScene node];
	GameViewLayer *layer = [GameViewLayer ShareInstance];
	[scene addChild: layer];
    [layer release];
	return scene;
}

+(GameViewLayer*) ShareInstance
{
    if (!g_cGameViewLayer)
    {
        g_cGameViewLayer = [[GameViewLayer alloc] init];
    }
    return g_cGameViewLayer;
}

+(void) setInfo:(int)nLevel money:(int)nMoney score:(int)nScore
{
    g_nLevel = nLevel;
    g_nScore = nScore;
    g_nRemainMoney = nMoney;
}

+(int) getRandomNumberBetweenMin:(int)min andMax:(int)max
{
    return ( (arc4random() % (max-min+1)) + min );
}

-(id) init
{
	if( (self = [super init]) ) {
        
        [[MainMenuLayer ShareInstance] removeBackSound];

        // ------------- set initial condition ----------- //
        
        UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(makepinch:)];  
        [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:pinch];
        
        m_nLevel = g_nLevel;
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        // ------------------------  end loading -------------------------- //
        
        [self attachEnvironment];
        [[MainMenuLayer ShareInstance] playBackSound:@"game_music"];
        appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

-(void) attachEnvironment
{
    // ------------- format values --------- //
    m_nFlag = 0;
    m_fDeviceScale = 0.5f;
    m_nMoney = nAryLevelMoney[m_nLevel - 1] + g_nRemainMoney;
    m_nTime = nAryLevelTime[m_nLevel - 1];
    m_bPause = NO;
    m_bExit = NO;
    m_nScore = g_nScore;
    m_fFirstPinch = 0.0f;
    m_bCreate = NO;
    
    [self schedule: @selector(addEnemyTime:) interval:nAryEnemyCreatTime[m_nLevel - 1]];
    [self schedule: @selector(discountTime:) interval:1];
    
    self.isTouchEnabled = YES;
    m_bSound = YES;
    m_nGold = nAryLevelGold[m_nLevel - 1];
    
    m_aryEnemy = [[NSMutableArray alloc] init];
    m_aryArcher = [[NSMutableArray alloc] init];
    m_aryFighter = [[NSMutableArray alloc] init];
    m_aryArrow = [[NSMutableArray alloc] init];
    
    m_archer = nil;
    m_fighter = nil;

    // ------------- add background -------- //
    
    backSprite = [CCSprite spriteWithFile:@"game.png"];
    backSprite.anchorPoint = CGPointZero;
    backSprite.scale = DEVICE_SCALE;
    [self addChild:backSprite z:LAYER_MAP];
    
    skySprite = [CCSprite spriteWithFile:@"sky.png"];
    [skySprite setPosition:ccp(DEVICE_WIDTH / 2, DEVICE_HEIGHT - skySprite.contentSize.height / 4)];
    skySprite.scale = DEVICE_SCALE;
    [self addChild:skySprite z:LAYER_GAME_BACK];

    underSkySprite = [CCSprite spriteWithFile:@"sky_bg.PNG"];
    [underSkySprite setPosition:ccp(underSkySprite.contentSize.width / 4 - DEVICE_WIDTH / 2, underSkySprite.contentSize.height / 4 + MAP_START_Y - 10)];
    underSkySprite.scale = DEVICE_SCALE;
    [self addChild:underSkySprite z:LAYER_GAME_BACK_SKY];

    g_tmxMap = nil;
    g_tmxMap = [CCTMXTiledMap tiledMapWithTMXFile:[NSString stringWithFormat:@"level%d_map.tmx", m_nLevel]];
    [g_tmxMap setPosition:ccp(MAP_START_X, MAP_START_Y)];
    g_tmxMap.scale = m_fDeviceScale;
    [self addChild:g_tmxMap z:LAYER_GAME_BACK];

    
    // -------------- add icon ------------------ //
    
    m_iconMedal = [CCSprite spriteWithFile:@"icon_medal.png"];
    [m_iconMedal setPosition:ccp(310, DEVICE_HEIGHT - 15)];
    m_iconMedal.scale = DEVICE_SCALE;
    [self addChild:m_iconMedal z:LAYER_ENVIRONMENT];
    
    m_iconTimer = [CCSprite spriteWithFile:@"icon_timer.png"];
    [m_iconTimer setPosition:ccp(255, DEVICE_HEIGHT - 15)];
    m_iconTimer.scale = DEVICE_SCALE;
    [self addChild:m_iconTimer z:LAYER_ENVIRONMENT];

    
    // -------------- add buttons --------------- //
    [self createButton];

    // -------------- add soldiers --------------- //

    for (int i = 0; i < 8; i ++)
    {

        CCSprite *spCharacter = [CCSprite spriteWithFile:@"list_1.png"];
        m_SModel[i] = [[SoldierModel alloc] init:i money:m_nMoney];
        [m_SModel[i] setPosition:ccp(17 + i * spCharacter.contentSize.width / 2, 15)];
        m_SModel[i].contentSize = CGSizeMake(35, 50);

        [self addChild:m_SModel[i] z:LAYER_ENVIRONMENT];
    }

    // -------------- add text ------------------- //
    [self updateLevelNumber];
    [self updateMoneyNumber:0];
    [self updateGoldNumber];
    [self updateScoreNumber:0];
    [self updateTimeNumber];
}

-(void) updateLevelNumber
{
    if (m_spLevel)
    {
        [m_spLevel removeAllChildrenWithCleanup:YES];
        [m_spLevel removeFromParentAndCleanup:YES];
        m_spLevel = nil;
    }
    
    int nLevel = m_nLevel;
    NSString *strNumber = [NSString stringWithFormat:@"%d", m_nLevel];
    if (m_nLevel == 31)
    {
        strNumber = @"1";
        nLevel = 1;
    }
    int nLength = 0;
    if (strNumber.length == 1)
        nLength = 9;
    
    strNumber = nil;
    
    if (m_nLevel != 31)
    {
        m_spLevel = [self attachInfo:nLevel type:1 xPos:TEXT_LEVEL_X + nLength yPos:TEXT_LEVEL_Y];
        [self addChild:m_spLevel z:LAYER_ENVIRONMENT];
    }
    else
    {
        m_spLevel = [CCSprite spriteWithFile:@"infinite.png"];
        m_spLevel.position = ccp(TEXT_LEVEL_X + 6, TEXT_LEVEL_Y);
        m_spLevel.scale = DEVICE_SCALE;
        [self addChild:m_spLevel z:LAYER_ENVIRONMENT];
    }
}

-(void) updateGoldNumber
{
    if (m_spGold)
    {
        [m_spGold removeAllChildrenWithCleanup:YES];
        [m_spGold removeFromParentAndCleanup:YES];
        m_spGold = nil;
    }
    
    NSString *strNumber = [NSString stringWithFormat:@"%d", m_nGold];
    int nLength = 0;
    if (strNumber.length == 1)
        nLength = 5;
    
    strNumber = nil;

    m_spGold = [self attachInfo:m_nGold type:1 xPos:TEXT_GOLD_X + nLength yPos:TEXT_GOLD_Y];
    [self addChild:m_spGold z:LAYER_ENVIRONMENT];
    
    [self attachJewel:m_nGold - 1 type:3 xPos:nAryLevelJewelPos[m_nLevel - 1][0] yPos:nAryLevelJewelPos[m_nLevel - 1][1]];
    
    if (m_nGold == 0)
    {
        m_bExit = YES;
        if (m_bPause == NO)
            [self pauseGame];
        [self showMessage:2];
        
        if (m_nLevel == 31)
        {
            [appDelegate increaseScore:m_nScore mode:1];
            
            GameSaveInfo *now_info = [[GameSaveInfo alloc] init];
            now_info.m_nLevel = 31;
            now_info.m_nMoney = m_nMoney;
            now_info.m_nScore = 0;
            now_info.m_nNewMoney = 0;
            
            GameSaver *gameSaver = [[GameSaver alloc] init];
            [gameSaver writeGameInfo:now_info];
            [gameSaver release];
            [now_info release];
        }
    }
}

-(void) updateMoneyNumber:(int)nMoney
{
    m_nMoney += nMoney;
    if (m_spMoney)
    {
        [m_spMoney removeAllChildrenWithCleanup:YES];
        [m_spMoney removeFromParentAndCleanup:YES];
        m_spMoney = nil;
    }

    NSString *strNumber = [NSString stringWithFormat:@"%d", m_nMoney];
    int nLength = 0;
    if (strNumber.length == 1)
        nLength = 10;
    if (strNumber.length == 2)
        nLength = 6;
    if (strNumber.length == 4)
        nLength = -5;
    if (strNumber.length == 5)
        nLength = -10;
    
    strNumber = nil;

    int tempMoney = m_nMoney;
    if (m_nMoney > 99999)
    {
        tempMoney = 99999;
    }

    m_spMoney = [self attachInfo:tempMoney type:1 xPos:TEXT_MONEY_X + nLength yPos:TEXT_MONEY_Y];
    [self addChild:m_spMoney z:LAYER_ENVIRONMENT];
}

-(void) updateScoreNumber:(int)nScore
{
    m_nScore += nScore;
    if (m_spScore)
    {
        [m_spScore removeAllChildrenWithCleanup:YES];
        [m_spScore removeFromParentAndCleanup:YES];
        m_spScore = nil;
    }

    m_spScore = [self attachInfo:m_nScore type:2 xPos:TEXT_SCORE_X yPos:TEXT_SCORE_Y];
    [self addChild:m_spScore z:LAYER_ENVIRONMENT];
}

-(void) updateTimeNumber
{
    if (m_spTime)
    {
        [m_spTime removeAllChildrenWithCleanup:YES];
        [m_spTime removeFromParentAndCleanup:YES];
        m_spTime = nil;
    }
    m_spTime = [self attachInfo:m_nTime type:2 xPos:TEXT_TIME_X yPos:TEXT_TIME_Y];
    [self addChild:m_spTime z:LAYER_ENVIRONMENT];
}

-(CCSprite*) attachInfo:(int)nValue type:(int)nType xPos:(int)nX yPos:(int)nY
{
    CCSprite *sprite = [self getCharacterSprite:nValue type:nType];
    [sprite setPosition:ccp(nX, nY)];
    return sprite;
}

-(void) attachJewel:(int)nValue type:(int)nType xPos:(int)nX yPos:(int)nY
{
    if (m_spJewel)
    {
        [m_spJewel removeAllChildrenWithCleanup:YES];
        [m_spJewel removeFromParentAndCleanup:YES];
        m_spJewel = nil;
    }
    m_spJewel = [self getCharacterSprite:nValue type:nType];
    [m_spJewel setPosition:ccp(nX * TILE_WIDTH, nY * TILE_HEIGHT - 2)];
    
    [g_tmxMap addChild:m_spJewel z:LAYER_ENTITY];
}

-(void) addEnemyTime: (ccTime) delta
{
    if (m_nLevel == 31)
    {
        if ((m_nTime >= 60) && (m_nTime < 120))
        {
            if (g_nAddFlag1 == 0)
            {
                [self unschedule:@selector(addEnemyTime:)];
                [self schedule:@selector(addEnemyTime:) interval:3];
                g_nAddFlag1 = 1;
            }
        }
        if ((m_nTime >= 180) && (m_nTime < 260))
        {
            if (g_nAddFlag2 == 0)
            {
                [self unschedule:@selector(addEnemyTime:)];
                [self schedule:@selector(addEnemyTime:) interval:2];
                g_nAddFlag2 = 1;
            }
        }
        if (m_nTime >= 300)
        {
            if (g_nAddFlag3 == 0)
            {
                [self unschedule:@selector(addEnemyTime:)];
                [self schedule:@selector(addEnemyTime:) interval:1];
                g_nAddFlag3 = 1;
            }
        }
    }

    [self createEnemy];
}

-(void) discountTime: (ccTime) delta
{
    if (m_nLevel == 31)
    {
        m_nTime ++;
    }
    else
    {
        m_nTime --;
    }
    
    [self updateTimeNumber];
    if (m_nTime == 0)
    {
        m_bExit = YES;
        if (m_bPause == NO)
            [self pauseGame];
        
        if (m_nLevel < 30)
        {
            GameSaveInfo *now_info = [[GameSaveInfo alloc] init];
            now_info.m_nLevel = m_nLevel + 1;
            now_info.m_nMoney = m_nMoney;
            now_info.m_nScore = m_nScore;
            now_info.m_nNewMoney = 0;
            
            GameSaver *gameSaver = [[GameSaver alloc] init];
            [gameSaver writeGameInfo:now_info];
            [gameSaver release];
            [now_info release];
            [self showMessage:1];
        }
        else
        {
            [self showMessage:4];
        }
        [appDelegate increaseScore:m_nScore + m_nMoney mode:0];
    }
}

-(void) createSoldier:(int)nType left:(int)nX top:(int)nY
{
    if (nType > 2)
    {
        m_nFlag = 1;
        int nNum = [m_aryArcher count];

        m_archer = [[Archer alloc] init:nType order:nNum];
        m_archer.scale = m_fDeviceScale;
        m_archer.position = ccp(nX, nY + OFFSET_TOUCH);
        [self addChild:m_archer z:LAYER_ENVIRONMENT];
        
    }
    else
    {
        m_nFlag = 1;
        int nNum = [m_aryFighter count];

        m_fighter = [[Fighter alloc] init:nType order:nNum];
        m_fighter.scale = m_fDeviceScale;
        m_fighter.position = ccp(nX, nY + OFFSET_TOUCH);
        [self addChild:m_fighter z:LAYER_ENVIRONMENT];

    }
}

-(void) createEnemy
{
    int nMax = nAryLevelMaxEnemy[m_nLevel - 1];
    int nMin = nAryLevelMinEnemy[m_nLevel - 1];
    
    if (m_nLevel == 31)
    {
        if (m_nTime < 30)
        {
            nMax = 2;
            nMin = 1;
        }
        if ((m_nTime >= 30) && (m_nTime < 60))
        {
            nMax = 3;
            nMin = 2;
        }
        if ((m_nTime >= 60) && (m_nTime < 90))
        {
            nMax = 4;
            nMin = 3;
        }
        if ((m_nTime >= 90) && (m_nTime < 120))
        {
            nMax = 5;
            nMin = 4;
        }
        if ((m_nTime >= 120) && (m_nTime < 180))
        {
            nMax = 6;
            nMin = 5;
        }
        if (m_nTime >= 180)
        {
            nMax = 6;
            nMin = 6;
        }
    }
    
    int nNum = [GameViewLayer getRandomNumberBetweenMin:nMin andMax:nMax];
    
    m_enemy = [[Enemy alloc] init:nNum load:0];
    [g_tmxMap addChild:m_enemy z:LAYER_ENTITY];
    [m_aryEnemy addObject:m_enemy];
    [m_enemy release];
}

-(void)makepinch:(UIPinchGestureRecognizer*)pinch
{
    // ------------------------------------ //
    if (m_archer)
    {
        [m_archer removeFromParentAndCleanup:YES];
        m_archer = nil;
    }
    if (m_fighter)
    {
        [m_fighter removeFromParentAndCleanup:YES];
        m_fighter = nil;
    }
    // ------------------------------------ //

    m_nFlag = 2;
    if(pinch.state == UIGestureRecognizerStateEnded)
    {
        m_nFlag = 0;
        if ((pinch.scale <= 1.0f) && (pinch.scale >= 0.5f))
        {
            m_fDeviceScale = pinch.scale;
            [g_tmxMap setScale:m_fDeviceScale];
        }
    }
    if(pinch.state == UIGestureRecognizerStateChanged)
    {
        if ((pinch.scale <= 1.0f) && (pinch.scale >= 0.5f))
        {
            m_fDeviceScale = pinch.scale;
            [g_tmxMap setScale:m_fDeviceScale];
        }
    }
    if(pinch.state == UIGestureRecognizerStateBegan && m_fDeviceScale != 0.0f)
    {
        pinch.scale = m_fDeviceScale;
        m_fFirstPinch = m_fDeviceScale;
    }
}

-(void) createButton
{
    item_music = [CCMenuItemImage itemFromNormalImage:@"btn_music.PNG" selectedImage:@"btn_music_down.PNG" target:self selector:@selector(clickMusic:)];
    item_music_disable = [CCMenuItemImage itemFromNormalImage:@"btn_music_1.PNG" selectedImage:@"btn_music_down.PNG" target:self selector:@selector(clickMusicDisable:)];
    item_repeat = [CCMenuItemImage itemFromNormalImage:@"btn_repeat.PNG" selectedImage:@"btn_repeat_down.PNG" target:self selector:@selector(clickRepeat:)];
    item_pause = [CCMenuItemImage itemFromNormalImage:@"btn_pouse.PNG" selectedImage:@"btn_pouse_down.PNG" target:self selector:@selector(clickPause:)];
    item_resume = [CCMenuItemImage itemFromNormalImage:@"btn_game_play.png" selectedImage:@"btn_game_play.png" target:self selector:@selector(clickPause:)];
    item_exit = [CCMenuItemImage itemFromNormalImage:@"btn_exit.PNG" selectedImage:@"btn_exit_down.PNG" target:self selector:@selector(clickExit:)];
    
    menu = [CCMenu menuWithItems: item_music, item_music_disable, item_repeat, item_pause, item_resume, item_exit, nil];
    menu.position = CGPointZero;
    
    item_music.scale = DEVICE_SCALE;
    item_music_disable.scale = DEVICE_SCALE;
    item_repeat.scale = DEVICE_SCALE;
    item_pause.scale = DEVICE_SCALE;
    item_resume.scale = DEVICE_SCALE;
    item_exit.scale = DEVICE_SCALE;
    
    item_music.position = ccp(MAP_START_X + 20, DEVICE_HEIGHT - 30);
    item_music_disable.position = ccp(MAP_START_X + 20, DEVICE_HEIGHT - 30);
    item_repeat.position = ccp(MAP_START_X + 60, DEVICE_HEIGHT - 30);
    item_pause.position = ccp(MAP_START_X + 100, DEVICE_HEIGHT - 30);
    item_resume.position = ccp(MAP_START_X + 100, DEVICE_HEIGHT - 30);
    item_exit.position = ccp(DEVICE_WIDTH - MAP_START_X * 2 - 30, DEVICE_HEIGHT - 15);
    
    if ([SettingViewLayer ShareInstance].m_bBackMusic == YES)
    {
        item_music.visible = YES;
        item_music_disable.visible = NO;
    }
    else
    {
        item_music.visible = NO;
        item_music_disable.visible = YES;
    }
    item_pause.visible = YES;
    item_resume.visible = NO;
    
    [self addChild: menu z:LAYER_ENVIRONMENT];
}

-(void) clickMusicDisable: (id) sender
{
    if (m_bExit == YES)
        return;

    m_bSound = !m_bSound;
    item_music.visible = YES;
    item_music_disable.visible = NO;

    [SettingViewLayer ShareInstance].m_bBackMusic = YES;
    [[MainMenuLayer ShareInstance] playBackSound:@"game_music"];
    [[SettingViewLayer ShareInstance] refreshCheckbox];
}

-(void) clickMusic: (id) sender
{
    if (m_bExit == YES)
        return;

    m_bSound = !m_bSound;
    item_music.visible = NO;
    item_music_disable.visible = YES;
    
    [SettingViewLayer ShareInstance].m_bBackMusic = NO;
    [[MainMenuLayer ShareInstance] removeBackSound];
    [[SettingViewLayer ShareInstance] refreshCheckbox];
}

-(void) clickRepeat: (id) sender
{
    if (m_bExit == YES)
        return;
    
    m_bExit = YES;
    
    if (m_bPause == NO)
        [self pauseGame];
    [self showMessage:3];
}

-(void) clickRepeatYes: (id) sender
{
    [self closeMessage];
    [self removeAllContents];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[GameViewLayer scene]]];    
}

-(void) clickPause: (id) sender
{
    if (m_bExit == YES)
        return;

    [self pauseGame];
}
-(void) clickExit: (id) sender
{
    if (m_bExit == YES)
        return;
    
    m_bExit = YES;

    if (m_bPause == NO)
        [self pauseGame];
    [self showMessage:0];
}

-(void) clickExitYes: (id) sender
{
    g_nRemainMoney = 0; // format remain money
    g_nScore = 0; // format score value

    [self closeMessage];
    [self removeAllContents];
    
    [[MainMenuLayer ShareInstance] removeBackSound];
    [[MainMenuLayer ShareInstance] playBackSound:@"menu_music"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[MainMenuLayer scene]]];
#ifdef FreeApp
    if (![SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        [[SNAdsManager sharedManager] giveMeThirdGameOverAd];
    }
#endif
}

-(void) clickExitNo: (id) sender
{
    m_bExit = NO;
    [self closeMessage];
    [self pauseGame];
#ifdef FreeApp
    if (![SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        [[SNAdsManager sharedManager] giveMeThirdGameOverAd];
    }
#endif
}

-(void) clickPlayNext: (id) sender
{
    g_nScore = m_nScore;
    g_nRemainMoney = m_nMoney;
    g_nLevel ++;
    [self closeMessage];
    [self removeAllContents];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[GameViewLayer scene]]];
#ifdef FreeApp
    if (![SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        [[SNAdsManager sharedManager] giveMeThirdGameOverAd];
    }
#endif
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    int nCheck = 0;
    NSSet *allTouches = [event allTouches];
    for( UITouch *touch in allTouches)
    {
        nCheck ++;
    }
    if (nCheck > 1)
        m_nFlag = 2;
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
     NSSet *allTouches = [event allTouches];
    
    if ([allTouches count] > 1)
    {
       return;
    }
    
   
    for( UITouch *touch in allTouches)
    {
        CGPoint start = [touch locationInView: [touch view]];	
        start = [[CCDirector sharedDirector] convertToGL: start];
        // ---------------- Map Moving ------------------ //
        if (m_nFlag == 0)
        {
            CGPoint end = [touch previousLocationInView:[touch view]];
            end = [[CCDirector sharedDirector] convertToGL:end];
            
            if ((end.y < MAP_START_Y) || (start.y < MAP_START_Y))
                return;
            
            
            CGPoint point = [g_tmxMap position];
            CGPoint skyPoint = [underSkySprite position];
            CGPoint tempPoint = point;
            
            CGFloat offset_x = (g_tmxMap.mapSize.width * TILE_WIDTH) * m_fDeviceScale - DEVICE_WIDTH;
            CGFloat offset_y = (g_tmxMap.mapSize.height * TILE_HEIGHT) * m_fDeviceScale - DEVICE_HEIGHT + MAP_START_Y;
            
            if (offset_x > 0)
            {
                point.x += start.x - end.x;
                if (point.x < (-1 * (MAP_START_X + offset_x))) point.x = -1 * (MAP_START_X + offset_x);
                if (point.x > MAP_START_X) point.x = MAP_START_X;
            }
            if (offset_y > 0)
            {
                point.y += start.y - end.y;
                if (point.y < (-1 * (MAP_START_Y + offset_y))) point.y = -1 * (MAP_START_Y + offset_y);
                if (point.y > MAP_START_Y) point.y = MAP_START_Y;
            }
            CGPoint offset_pos = ccpSub(tempPoint, point);
            offset_pos = ccp(offset_pos.x / 15, offset_pos.y / 15);
            [underSkySprite setPosition:ccpSub(skyPoint, offset_pos)];
            [g_tmxMap setPosition:point];
        }
        
        // ------------------ moving character after touch soldier model ----------------- //
        if (m_nFlag == 1)
        {
            if (m_archer)
                m_archer.position = ccp(start.x, start.y + OFFSET_TOUCH);
            if (m_fighter)
                m_fighter.position = ccp(start.x, start.y + OFFSET_TOUCH);
            
            // --------- if it's not available seat, remove it ------------- //

            if (m_archer)
            {
                CGFloat x = m_archer.position.x - g_tmxMap.position.x;
                CGFloat y = m_archer.position.y - g_tmxMap.position.y;
                x = x / m_fDeviceScale;
                y = y / m_fDeviceScale;
                
                for (int i = 0; i < y; i ++)
                {
                    CGPoint ptPos = [m_archer tilePosFromLocation:ccp(x, y - i - TILE_HEIGHT - OFFSET)];
                    int result = [m_archer isBlock:ptPos];
                    
                    if (result > DIR_NOTHING)
                    {
                        if ((result != DIR_NOTHING) && (result != DIR_UP_SEAT) && 
                            (result != DIR_STR_SEAT) && (result != DIR_DOWN_SEAT) &&
                            (result != DIR_LTR_SEAT) && (result != DIR_RTL_SEAT))
                        {
                            
                            // ---------- if any character occupy already this seat --------- //
                            BOOL bCheck = NO;
                            for (int nNum = 0; nNum < [m_aryArcher count]; nNum ++)
                            {
                                Archer *archer = [m_aryArcher objectAtIndex:nNum];
                                
                                if (archer.m_bDie == YES)
                                    continue;
                                
                                if (nNum == [m_aryArcher indexOfObject:m_archer])
                                    continue;
                                
                                if ((x > archer.position.x - OFFSET_OCCUPY_X) && 
                                    (x < archer.position.x + OFFSET_OCCUPY_X) && 
                                    (y - i - TILE_HEIGHT - OFFSET > archer.position.y - OFFSET_OCCUPY_Y - OFFSET) && 
                                    (y - i - TILE_HEIGHT - OFFSET < archer.position.y + OFFSET_OCCUPY_Y - OFFSET))
                                {
                                    bCheck = YES;
                                    m_archer.m_bSeat = NO;
                                    [m_archer attachNoMove];
                                    return;
                                }
                            }
                            for (int nNum = 0; nNum < [m_aryFighter count]; nNum ++)
                            {
                                Fighter *fighter = [m_aryFighter objectAtIndex:nNum];
                                
                                if (fighter.m_bDie == YES)
                                    continue;
                                
                                if ((x > fighter.position.x - OFFSET_OCCUPY_X) && 
                                    (x < fighter.position.x + OFFSET_OCCUPY_X) && 
                                    (y - i - TILE_HEIGHT - OFFSET > fighter.position.y - OFFSET_OCCUPY_Y - OFFSET) && 
                                    (y - i - TILE_HEIGHT - OFFSET < fighter.position.y + OFFSET_OCCUPY_Y - OFFSET))
                                {
                                    bCheck = YES;
                                    m_archer.m_bSeat = NO;
                                    [m_archer attachNoMove];
                                    return;
                                }
                            }
                            if (bCheck == NO)
                            {
                                m_archer.m_bSeat = YES;
                                [m_archer removeNoMove];
                            }
                            else
                            {
                                m_archer.m_bSeat = NO;
                                [m_archer attachNoMove];
                            }
                        }
                        else
                        {
                            m_archer.m_bSeat = NO;
                            [m_archer attachNoMove];
                        }
                        return;
                    }
                }
            }
            if (m_fighter)
            {
                CGFloat x = m_fighter.position.x - g_tmxMap.position.x;
                CGFloat y = m_fighter.position.y - g_tmxMap.position.y;
                x = x / m_fDeviceScale;
                y = y / m_fDeviceScale;
                
                for (int i = 0; i < y; i ++)
                {

                    CGPoint ptPos = [m_fighter tilePosFromLocation:ccp(x, y - i - TILE_HEIGHT - OFFSET)];
                    int result = [m_fighter isBlock:ptPos];

                    if (result > DIR_NOTHING)
                    {
                        if ((result != DIR_NOTHING) && (result != DIR_UP_SEAT) && 
                            (result != DIR_STR_SEAT) && (result != DIR_DOWN_SEAT) &&
                            (result != DIR_LTR_SEAT) && (result != DIR_RTL_SEAT))
                        {
                            // ---------- if any character occupy already this seat --------- //
                            BOOL bCheck = NO;
                            for (int nNum = 0; nNum < [m_aryArcher count]; nNum ++)
                            {
                                Archer *archer = [m_aryArcher objectAtIndex:nNum];
                                
                                if (archer.m_bDie == YES)
                                    continue;

                                if ((x > archer.position.x - OFFSET_OCCUPY_X) && 
                                    (x < archer.position.x + OFFSET_OCCUPY_X) &&
                                    (y - i - TILE_HEIGHT - OFFSET > archer.position.y - OFFSET_OCCUPY_Y - OFFSET) && 
                                    (y - i - TILE_HEIGHT - OFFSET < archer.position.y + OFFSET_OCCUPY_Y - OFFSET))
                                {
                                    bCheck = YES;
                                    m_fighter.m_bSeat = NO;
                                    [m_fighter attachNoMove];
                                    return;
                                }
                            }
                            for (int nNum = 0; nNum < [m_aryFighter count]; nNum ++)
                            {
                                Fighter *fighter = [m_aryFighter objectAtIndex:nNum];
                                
                                if (nNum == [m_aryFighter indexOfObject:m_fighter])
                                    continue;
                                
                                if (fighter.m_bDie == YES)
                                    continue;

                                if ((x > fighter.position.x - OFFSET_OCCUPY_X) && 
                                    (x < fighter.position.x + OFFSET_OCCUPY_X) && 
                                    (y - i - TILE_HEIGHT - OFFSET > fighter.position.y - OFFSET_OCCUPY_Y - OFFSET) && 
                                    (y - i - TILE_HEIGHT - OFFSET < fighter.position.y + OFFSET_OCCUPY_Y - OFFSET))
                                {
                                    bCheck = YES;
                                    m_fighter.m_bSeat = NO;
                                    [m_fighter attachNoMove];
                                    return;
                                }
                            }
                            if (bCheck == NO)
                            {
                                m_fighter.m_bSeat = YES;
                                [m_fighter removeNoMove];
                            }
                            else
                            {
                                m_fighter.m_bSeat = NO;
                                [m_fighter attachNoMove];
                            }
                        }
                        else
                        {
                            m_fighter.m_bSeat = NO;
                            [m_fighter attachNoMove];
                        }
                        return;
                    }
                }
            }
        }
    }
}

-(void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    m_bCreate = NO;
    if (m_nFlag != 1)
    {
        if (m_archer)
        {
            [m_archer removeFromParentAndCleanup:YES];
            m_archer = nil;
            return;
        }
        if (m_fighter)
        {
            [m_fighter removeFromParentAndCleanup:YES];
            m_fighter = nil;
            return;
        }
    }
    if (m_nFlag == 1)
    {
        if (m_archer)
        {
            if ((m_archer.position.x < MAP_START_X) || (m_archer.position.x > DEVICE_WIDTH * 2 - MAP_START_X) || (m_archer.position.y < (MAP_START_Y + 50)) || (m_archer.m_bSeat == NO))
            {
                [m_archer removeFromParentAndCleanup:YES];
                m_archer = nil;
                return;
            }
            else
            {
                [self removeChild:m_archer cleanup:YES];
                CGFloat x = m_archer.position.x - g_tmxMap.position.x;
                CGFloat y = m_archer.position.y - g_tmxMap.position.y;
                x = x / m_fDeviceScale;
                y = y / m_fDeviceScale;
                m_archer.position = ccp(x, y);
                m_archer.scale = 1.0f;
                [g_tmxMap addChild:m_archer z:LAYER_ENTITY];
                [m_archer startAnimaion];
                
                // -------- put archer to array ----------- //
                [m_aryArcher addObject:m_archer];
                [m_archer release];
                


                m_nMoney -= nArySoldierGold[m_archer.m_nType];
                [m_archer removeSign];
                m_archer = nil;
            }
        }
        if (m_fighter)
        {
            if ((m_fighter.position.x < MAP_START_X) || (m_fighter.position.x > DEVICE_WIDTH * 2 - MAP_START_X) || (m_fighter.position.y < (MAP_START_Y + 50)) || (m_fighter.m_bSeat == NO))
            {
                [m_fighter removeFromParentAndCleanup:YES];
                m_fighter = nil;
                return;
            }
            else
            {
                [self removeChild:m_fighter cleanup:YES];
                CGFloat x = m_fighter.position.x - g_tmxMap.position.x;
                CGFloat y = m_fighter.position.y - g_tmxMap.position.y;
                x = x / m_fDeviceScale;
                y = y / m_fDeviceScale;
                m_fighter.position = ccp(x, y);
                m_fighter.scale = 1.0f;
                [g_tmxMap addChild:m_fighter z:LAYER_ENTITY];
                [m_fighter startAnimaion];
                
                // -------- put fighter to array ----------- //
                [m_aryFighter addObject:m_fighter];
                [m_fighter release];

                
                m_nMoney -= nArySoldierGold[m_fighter.m_nType];
                [m_fighter removeSign];
                m_fighter = nil;
            }
        }
        [self reshowCharacter];
        [self updateMoneyNumber:0];
    }
    m_nFlag = 0;
}

-(void) reshowCharacter
{
    // -------------- check soldier model --------------- //
    
    for (int i = 0; i < 8; i ++)
    {
        [m_SModel[i] checkCharacter:m_nMoney];
    }
}

-(CCSprite*) getCharacterSprite:(int)value type:(int)nType
{
    CCSprite* spInfo = [CCSprite node];
    UIImage *img_level;
    if (nType == 1)
        img_level = [UIImage imageNamed:@"number1.png"];
    else if (nType == 2)
        img_level = [UIImage imageNamed:@"number2.png"];
    else if (nType == 3)
        img_level = [UIImage imageNamed:@"coin.png"];
    
    int tmpLevel = value;
    
    int aryLevel[10];
    for (int i = 0; i < 10; i ++)
        aryLevel[i] = 0;
    int nIndex = 0;
    
    while (tmpLevel >= 0) {
        aryLevel[nIndex] = tmpLevel % 10;
        tmpLevel = tmpLevel / 10;
        nIndex ++;
        
        if (tmpLevel == 0)
            break;
    }
    
    
    CustomLabel *newImgLevel = [[CustomLabel alloc] init:img_level.size.width img_height:img_level.size.height sepa_width:10];
    CGFloat sepa_width = img_level.size.width / 10;
    
    for (int i = 0; i < nIndex; i ++)
    {
        UIImage *imgSepa = [newImgLevel getPara_CustomFont:img_level nOrder:aryLevel[nIndex - i - 1]];
        CCTexture2D *texture = [[[CCTexture2D alloc] initWithImage:imgSepa] autorelease];
        CCSprite *spSepa = [CCSprite spriteWithTexture:texture];
        [spSepa setPosition:ccp(i * sepa_width / 2, 0)];
        spSepa.scale = DEVICE_SCALE;
        [spInfo addChild:spSepa z:LAYER_ENVIRONMENT];
    }
    [newImgLevel release];
    newImgLevel = nil;
    return spInfo;
}


-(void) pauseGame
{
    m_bPause = !m_bPause;

    if (m_bPause == YES)
    {
        item_pause.visible = NO;
        item_resume.visible = YES;
        [self pauseSchedulerAndActions];
        for (int i = 0; i < [m_aryEnemy count]; i ++)
        {
            Enemy *enemy = [m_aryEnemy objectAtIndex:i];
            [enemy pauseSchedulerAndActions];
        }
        for (int i = 0; i < [m_aryArcher count]; i ++)
        {
            Archer *archer = [m_aryArcher objectAtIndex:i];
            if (archer.m_bDie == NO)
            {
                [archer pauseSchedulerAndActions];
                [archer.m_spUp pauseSchedulerAndActions];
            }
        }
        for (int i = 0; i < [m_aryFighter count]; i ++)
        {
            Fighter *fighter = [m_aryFighter objectAtIndex:i];
            [fighter pauseSchedulerAndActions];
        }
        for (int i = 0; i < [m_aryArrow count]; i ++)
        {
            Arrow *arrow = [m_aryArrow objectAtIndex:i];
            if (arrow)
            {
                [arrow pauseSchedulerAndActions];
            }
        }
    }
    if (m_bPause == NO)
    {
        item_pause.visible = YES;
        item_resume.visible = NO;
        [self resumeSchedulerAndActions];
        for (int i = 0; i < [m_aryEnemy count]; i ++)
        {
            Enemy *enemy = [m_aryEnemy objectAtIndex:i];
            [enemy resumeSchedulerAndActions];
        }
        for (int i = 0; i < [m_aryArcher count]; i ++)
        {
            Archer *archer = [m_aryArcher objectAtIndex:i];
            if (archer.m_bDie == NO)
            {
                [archer resumeSchedulerAndActions];
                [archer.m_spUp resumeSchedulerAndActions];
            }
        }
        for (int i = 0; i < [m_aryFighter count]; i ++)
        {
            Fighter *fighter = [m_aryFighter objectAtIndex:i];
            [fighter resumeSchedulerAndActions];
        }
        for (int i = 0; i < [m_aryArrow count]; i ++)
        {
            Arrow *arrow = [m_aryArrow objectAtIndex:i];
            if (arrow)
            {
                [arrow resumeSchedulerAndActions];
            }
        }

    }
}

-(void) showMessage:(int)nType
{
    //overLayer = [[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 128)];
    //[self addChild:overLayer z:100];
    
    spBack = [CCSprite spriteWithFile:@"message_bg.png"];
    spBack.anchorPoint = CGPointZero;
    spBack.position = ccp(90, 50);
    spBack.scale = DEVICE_SCALE;
    [self addChild:spBack z:LAYER_MESSAGE];
    
    if (nType == 0) // exit game
    {
        spMsg = [CCSprite spriteWithFile:@"are_u_sure.png"];
        spMsg.position = ccp(230, 190);
        item_yes = [CCMenuItemImage itemFromNormalImage:@"rbtn_yes.PNG" selectedImage:@"rbtn_yes1.PNG" target:self selector:@selector(clickExitYes:)];
        item_no = [CCMenuItemImage itemFromNormalImage:@"rbtn_no.PNG" selectedImage:@"rbtn_no1.PNG" target:self selector:@selector(clickExitNo:)];
        item_yes.position = ccp(190, 140);
        item_no.position = ccp(270, 140);
        msg_menu = [CCMenu menuWithItems: item_yes, item_no, nil];
        item_yes.scale = DEVICE_SCALE;
        item_no.scale = DEVICE_SCALE;
    }
    if (nType == 1) // you win
    {
        spMsg = [CCSprite spriteWithFile:@"u_win.png"];
        spMsg.position = ccp(230, 190);
        item_yes = [CCMenuItemImage itemFromNormalImage:@"rbtn_play.PNG" selectedImage:@"rbtn_play1.PNG" target:self selector:@selector(clickPlayNext:)];
        item_yes.position = ccp(230, 140);
        msg_menu = [CCMenu menuWithItems: item_yes, nil];
        item_yes.scale = DEVICE_SCALE;
    }
    if (nType == 2) // you lose
    {
        spMsg = [CCSprite spriteWithFile:@"u_lose.png"];
        spMsg.position = ccp(230, 190);
        item_yes = [CCMenuItemImage itemFromNormalImage:@"rbtn_restart.PNG" selectedImage:@"rbtn_restart1.PNG" target:self selector:@selector(clickRepeatYes:)];
        item_no = [CCMenuItemImage itemFromNormalImage:@"rbtn_exit.PNG" selectedImage:@"rbtn_exit1.PNG" target:self selector:@selector(clickExitYes:)];
        item_yes.position = ccp(190, 140);
        item_no.position = ccp(270, 140);
        msg_menu = [CCMenu menuWithItems: item_yes, item_no, nil];
        item_yes.scale = DEVICE_SCALE;
        item_no.scale = DEVICE_SCALE;
    }
    if (nType == 3) // restart game
    {
        spMsg = [CCSprite spriteWithFile:@"are_u_sure.png"];
        spMsg.position = ccp(230, 190);
        item_yes = [CCMenuItemImage itemFromNormalImage:@"rbtn_yes.PNG" selectedImage:@"rbtn_yes1.PNG" target:self selector:@selector(clickRepeatYes:)];
        item_no = [CCMenuItemImage itemFromNormalImage:@"rbtn_no.PNG" selectedImage:@"rbtn_no1.PNG" target:self selector:@selector(clickExitNo:)];
        item_yes.position = ccp(190, 140);
        item_no.position = ccp(270, 140);
        msg_menu = [CCMenu menuWithItems: item_yes, item_no, nil];
        item_yes.scale = DEVICE_SCALE;
        item_no.scale = DEVICE_SCALE;
    }
    if (nType == 4) // Exit game
    {
        spMsg = [CCSprite spriteWithFile:@"u_win.png"];
        spMsg.position = ccp(230, 190);
        item_yes = [CCMenuItemImage itemFromNormalImage:@"rbtn_exit.PNG" selectedImage:@"rbtn_exit1.PNG" target:self selector:@selector(clickExitYes:)];
        item_yes.position = ccp(230, 140);
        msg_menu = [CCMenu menuWithItems: item_yes, nil];
        item_yes.scale = DEVICE_SCALE;
    }


    msg_menu.position = CGPointZero;
    spMsg.scale = DEVICE_SCALE;
    [self addChild:spMsg z:LAYER_MESSAGE + 1];
    [self addChild:msg_menu z:LAYER_MESSAGE + 2];
}

-(void) closeMessage
{
    //[overLayer removeFromParentAndCleanup:YES];
    
    [spBack removeFromParentAndCleanup:YES];
    
    [spMsg removeFromParentAndCleanup:YES];
    
    [self removeChild:item_yes cleanup:YES];
    [self removeChild:item_no cleanup:YES];
    [self removeChild:msg_menu cleanup:YES];

    spMsg = nil;
    spBack = nil;
    //overLayer = nil;
    item_yes = nil;
    item_no = nil;
    msg_menu = nil;
}

-(void) removeAllContents
{
    // ------------- remove all enemy --------------- //
    for (int i = 0; i < [m_aryEnemy count]; i ++)
    {
        Enemy *enemy = [m_aryEnemy objectAtIndex:0];
        if (enemy.m_bDie == NO)
        {
            [enemy unscheduleAllSelectors];
            [enemy removeAllChildrenWithCleanup:YES];
            [enemy removeFromParentAndCleanup:YES];
        }
        [m_aryEnemy removeObject:enemy];
        enemy = nil;
    }
    [m_aryEnemy release];
    m_aryEnemy = nil;
    
    // ------------- remove all archer --------------- //
    for (int i = 0; i < [m_aryArcher count]; i ++)
    {
        Archer *archer = [m_aryArcher objectAtIndex:0];
        
        if (archer.m_bDie == NO)
        {
            [archer unscheduleAllSelectors];
            [archer removeAllChildrenWithCleanup:YES];
        }
        [archer removeFromParentAndCleanup:YES];
        [m_aryArcher removeObject:archer];
        archer = nil;
    }
    [m_aryArcher release];
    m_aryArcher = nil;
    
    // ------------- remove all fighter --------------- //
    for (int i = 0; i < [m_aryFighter count]; i ++)
    {
        Fighter *fighter = [m_aryFighter objectAtIndex:0];
        if (fighter.m_bDie == NO)
        {
            [fighter unscheduleAllSelectors];
            [fighter removeAllChildrenWithCleanup:YES];
            [fighter removeFromParentAndCleanup:YES];
        }
        [m_aryFighter removeObject:fighter];
        fighter = nil;
    }
    [m_aryFighter release];
    m_aryFighter = nil;
    
    // ------------- remove all arrow --------------- //
    [m_aryArrow removeAllObjects];
    [m_aryArrow release];
    m_aryArrow = nil;
    
    [backSprite removeFromParentAndCleanup:YES];
    backSprite = nil;
    [skySprite removeFromParentAndCleanup:YES];
    skySprite = nil;
    [underSkySprite removeFromParentAndCleanup:YES];
    underSkySprite = nil;

    // ------------------------------------------------ //
    [self cleanup];
    [self unscheduleAllSelectors];

    [self removeAllChildrenWithCleanup:YES];
    [self removeFromParentAndCleanup:YES];
    g_cGameViewLayer = nil;
}


-(void) cleanup {
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
    [super cleanup];
}

-(void) dealloc
{
	[super dealloc];
}

@end
