//
//  SoldierModel.m
//  Defence
//
//  Created by huyingan on 12/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SoldierModel.h"
#import "GameViewLayer.h"

@implementation SoldierModel

-(id) init:(int)nType money:(int)nMoney
{
	if( (self = [super init]) ) {
        
        self.isTouchEnabled = YES;
        m_nType = nType;
        m_nMoney = nArySoldierGold[nType];

        NSString *strName = [NSString stringWithFormat:@"list_act_%d.png", nType + 1];
        
        if (m_nMoney > nMoney)
        {
            strName = [NSString stringWithFormat:@"list_%d.png", nType + 1];
        }

        m_spCharacter = [CCSprite spriteWithFile:strName];
        strName = nil;
        
        [m_spCharacter setPosition:ccp(m_spCharacter.contentSize.width / 4, 20)];
        [self addChild:m_spCharacter z:LAYER_ENVIRONMENT];
        self.scale = DEVICE_SCALE;

    }
    return self;
}

-(void) checkCharacter:(int)nMoney
{
    [m_spCharacter removeFromParentAndCleanup:YES];

    NSString *strName = [NSString stringWithFormat:@"list_act_%d.png", m_nType + 1];
    
    if (m_nMoney > nMoney)
    {
        strName = [NSString stringWithFormat:@"list_%d.png", m_nType + 1];
    }
    
    m_spCharacter = [CCSprite spriteWithFile:strName];
    
    [m_spCharacter setPosition:ccp(m_spCharacter.contentSize.width / 4, 20)];
    [self addChild:m_spCharacter z:LAYER_ENVIRONMENT];
    strName = nil;
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([GameViewLayer ShareInstance].m_bCreate == YES)
        return;
    
    if (m_nMoney > [GameViewLayer ShareInstance].m_nMoney)
        return;

    NSSet *allTouches = [event allTouches];
    
    if ([allTouches count] > 1)
    {
        return;
    }
    
    for( UITouch *touch in allTouches)
    {
        // ---------------- Map Moving ------------------ //
        CGPoint start = [touch locationInView: [touch view]];	
        start = [[CCDirector sharedDirector] convertToGL: start];
        if ((start.x > self.position.x) && (start.x < self.position.x + self.contentSize.width) &&
            (start.y > self.position.y) && (start.y < self.position.y + self.contentSize.height))
        {
            [[GameViewLayer ShareInstance] createSoldier:m_nType left:start.x top:start.y];
            [GameViewLayer ShareInstance].m_bCreate = YES;
        }
    }
}

-(void) cleanupCharater 
{
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
    [super cleanup];
}

-(void) dealloc
{
    [self cleanupCharater];
	[super dealloc];
}


@end
