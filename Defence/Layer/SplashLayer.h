//
//  MainMenuLayer.h
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SplashLayer : CCLayer
{
    
    CCSprite        *backSprite;
}

+(id) scene;
+(SplashLayer*) ShareInstance;

@end
