//
//  Enemy.h
//  Defence
//
//  Created by huyingan on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entity.h"
#import "ConstValue.h"
#import "Arrow.h"

@interface Enemy : Entity {
    
    int             m_nType;
    int             m_nLevel;
    int             m_nDir;   // 1 : left to right, -1 : right to left
    int             m_nCheck; // now action status
    int             m_nAction;
    int             m_nHealth;
    int             m_nAttack;
    NSMutableArray  *m_aryArrow;
    
    int             m_nRandTemp;
    BOOL            m_bRand;
    BOOL            m_bDie;
    int             m_nDieCount;
    int             m_nFighter;
    int             m_nArcher;
    BOOL            m_bFightFlag;
    int             m_nFightSoldier;
    
    int             m_nSoundFlag;
    int             m_nSpeedFlag;
}

-(id) init:(int)nType load:(int)nLoad;
-(void) attachImage:(int)nLoad;
-(void) moveCharacter;
-(void) setAnimation:(int)nAction repeat:(BOOL)bForever;
-(int) isBlock: (CGPoint) tilePos;
-(CGPoint) tilePosFromLocation:(CGPoint) ptPos;
-(void) startFight;
-(void) hittedArrow:(Arrow*)arrow posX:(CGFloat)nX posY:(CGFloat)nY;
-(void) dieEnemy;
-(void) removeAllEnemyInfo;
-(void) cleanupCharater;
-(void) selectFighter;
-(void) selectArcher;

@property (readwrite) int m_nDir;
@property (readwrite) int m_nHealth;
@property (readwrite) int m_nAttack;
@property (readwrite) BOOL m_bDie;
@property (readwrite) BOOL m_bFightFlag;
@property (readwrite) int m_nFighter;
@property (readwrite) int m_nArcher;
@property (readwrite) int m_nType;

@end
