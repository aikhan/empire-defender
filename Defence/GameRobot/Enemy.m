//
//  Enemy.m
//  Defence
//
//  Created by huyingan on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Enemy.h"
#import "GameViewLayer.h"
#import "MainMenuLayer.h"

@implementation Enemy

@synthesize m_nDir;
@synthesize m_nHealth;
@synthesize m_nAttack;
@synthesize m_bDie;
@synthesize m_nFighter;
@synthesize m_nArcher;
@synthesize m_nType;
@synthesize m_bFightFlag;

-(id) init:(int)nType load:(int)nLoad
{
	if( (self=[super init] )) {

        m_nDir = 1;
        m_nCheck = ACTION_NOTHING;     // check running action
        m_nType = nType;    // enemy type : 1 ~ 6
        m_nRandTemp = 0;
        m_bRand = NO;
        m_bDie = NO;
        m_nFighter = -1;
        m_nArcher = -1;

        m_nHealth = nAryEnemyHealth[nType - 1];
        m_nAttack = nAryEnemyAttack[nType - 1];
        
        [self attachImage:nLoad];
        //[self schedule: @selector(update:) interval:nAryEnemySpeed[m_nType - 1]];
        [self scheduleUpdate];
        
        m_bFightFlag = NO;
        m_nDieCount = 100;
        m_aryArrow = [[NSMutableArray alloc] init];
        
        m_nSoundFlag = 0;
        m_nSpeedFlag = 0;
	}
	return self;
}

-(void) attachImage:(int)nLoad
{
    // set position of enemy with level information
    if (nLoad == 0)
        m_nLevel = [GameViewLayer ShareInstance].m_nLevel;
    else
        m_nLevel = 1;
    int nCountDoor = nAryLevelDoorCount[m_nLevel - 1];
    int nDoor = [GameViewLayer getRandomNumberBetweenMin:0 andMax:nCountDoor - 1];
    int nDir = nAryStartDir[m_nLevel - 1][nDoor];
    
    
    [self setAnimation:ACTION_READY repeat:YES];

    CGFloat x = nAryStartPoint[m_nLevel - 1][nDoor][0] * TILE_WIDTH + OFFSET;
    CGFloat y = nAryStartPoint[m_nLevel - 1][nDoor][1] * TILE_HEIGHT + OFFSET;
    self.position = ccp(x, y);
    self.m_nDir = nDir;
    if (nDir == -1)
        self.flipX = 180;
}

-(void) update: (ccTime) delta
{
    m_nSpeedFlag ++;
    if (m_nSpeedFlag == nAryEnemySpeed[m_nType - 1])
    {
        m_nArcher = -1;
        m_nFighter = -1;
        [self moveCharacter];
        [self selectFighter];
        [self selectArcher];
        
        m_nSpeedFlag = 0;
    }
}

-(void) dieEnemy
{
    if (m_nArcher != -1)
    {
        Archer* archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:m_nArcher];
        if (archer.m_bDie == NO)
            archer.m_bFightFlag = NO;
    }
    if (m_nFighter != -1)
    {
        Fighter* fighter = [[GameViewLayer ShareInstance].m_aryFighter objectAtIndex:m_nFighter];
        if (fighter.m_bDie == NO)
        {
            [fighter unscheduleAllSelectors];
            fighter.m_bFightFlag = NO;
            fighter.m_nAction = ACTION_READY;
            [fighter setAnimation:ACTION_READY repeat:YES];
        }
    }
    for (int i = 0; i < [m_aryArrow count]; i ++)
    {
        Arrow *arrow = [m_aryArrow objectAtIndex:0];
        if (arrow)
        {
            [arrow unscheduleAllSelectors];
            [self removeChild:arrow cleanup:YES];
            [m_aryArrow removeObject:arrow];
            arrow = nil;
        }
    }
    [m_aryArrow release];
    m_aryArrow = nil;
    
    m_nArcher = -1;
    m_nFighter = -1;
    
    [[GameViewLayer ShareInstance] updateMoneyNumber:nAryEnemyScore[m_nType - 1]];
    [[GameViewLayer ShareInstance] updateScoreNumber:nAryEnemyScore[m_nType - 1]];
    [[GameViewLayer ShareInstance] reshowCharacter];
    
    

    [self unscheduleAllSelectors];
    [self removeAllEnemyInfo];
    [self schedule: @selector(removeEnemy:) interval:0.1];
}

-(void) removeAllEnemyInfo
{
    int nNum = [[GameViewLayer ShareInstance].m_aryEnemy indexOfObject:self];
    
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryArcher count]; i ++)
    {
        Archer* archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:i];
        
        if (archer.m_bDie == YES)
            continue;
        /*
        if (archer.m_nSelEnemy > nNum)
        {
            archer.m_nSelEnemy --;
        }
        */
        if (archer.m_nSelEnemy == nNum)
        {
            archer.m_nSelEnemy = -1;
        }
    }
    
    //[[GameViewLayer ShareInstance].m_aryEnemy removeObject:self];
}

-(void) removeEnemy: (ccTime) delta
{
    m_nDieCount -= 5;
    [self setOpacity:m_nDieCount];
    if (m_nDieCount < 0)
    {
        [self unscheduleAllSelectors];
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
    }
}

-(void) startFight
{
    [self setAnimation:ACTION_READY repeat:NO];
    
    if (m_nFighter != -1)
    {
        m_nAction = ACTION_FIGHT1;
    }
    if (m_nArcher != -1)
    {
        m_nAction = ACTION_FIGHT2;
    }

    [self schedule: @selector(fighting:) interval:ACTION_SPEED];
}

-(void) fighting: (ccTime) delta
{
    [self setAnimation:m_nAction repeat:NO];
    
    if (m_nFighter != -1)
    {
        m_nAction ++;
        if (m_nAction == ACTION_READY)
            m_nAction = ACTION_FIGHT1;

        if (m_nSoundFlag > 0)
            [[MainMenuLayer ShareInstance] playEffectSound:@"sword" nX:self.position.x nY:self.position.y];

        // ------------------------ calculate health of enemy ------------------------ //
        
        Fighter *fighter = [[GameViewLayer ShareInstance].m_aryFighter objectAtIndex:m_nFighter];
        if ((m_nAction == ACTION_FIGHT2) || (m_nAction == ACTION_FIGHT4) || (m_nAction == ACTION_FIGHT6))
        {
            m_nHealth -= fighter.m_nAttack;
        }
        else
        {
            fighter.m_nHealth -= m_nAttack;
        }
        
        if (m_nSoundFlag > 0)
        {
            if (m_nHealth < 0)
            {
                [self unscheduleAllSelectors];
                [self setAnimation:ACTION_DIE repeat:NO];
                m_bDie = YES;
                [self dieEnemy];
                
                fighter.m_bFightFlag = NO;
                fighter.m_nAction = ACTION_READY;
                [fighter unscheduleAllSelectors];
                [fighter setAnimation:ACTION_READY repeat:YES];
                m_nSoundFlag = 0;
                return;
            }
            
            if (fighter.m_nHealth < 0)
            {
                [self unscheduleAllSelectors];
                
                [fighter unscheduleAllSelectors];
                [fighter dieFighter];
                fighter.m_bDie = YES;
                //[self schedule: @selector(update:) interval:nAryEnemySpeed[m_nType - 1]];
                [self scheduleUpdate];
                
                m_nFighter = -1;
                m_nArcher = -1;
                m_bFightFlag = NO;
                m_nSoundFlag = 0;
                return;
            }
        }
        m_nSoundFlag = 1;

        // --------------------------------------------------------------------------- //
    }
    
    if (m_nArcher != -1)
    {
        m_nAction += 2;
        if (m_nAction >= ACTION_READY)
            m_nAction = ACTION_FIGHT2;
        
        // ------------------------ calculate health of archer ------------------------ //
        
        if (m_nSoundFlag > 0)
            [[MainMenuLayer ShareInstance] playEffectSound:@"sword" nX:self.position.x nY:self.position.y];
        
        Archer *archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:m_nArcher];
        if ((m_nAction == ACTION_FIGHT2) || (m_nAction == ACTION_FIGHT4) || (m_nAction == ACTION_FIGHT6))
        {
            if (m_nSoundFlag > 0)
            {
                archer.m_nHealth -= m_nAttack;
                if (archer.m_nHealth < 0)
                {
                    m_nArcher = -1;
                    m_nFighter = -1;
                    m_bFightFlag = NO;
                    [self unscheduleAllSelectors];
                    [archer dieArcher];
                    //[self schedule: @selector(update:) interval:nAryEnemySpeed[m_nType - 1]];
                    [self scheduleUpdate];
                    m_nSoundFlag = 0;
                }
            }
            m_nSoundFlag = 1;
        }
        // --------------------------------------------------------------------------- //
    }
}

-(void) setAnimation:(int)nAction repeat:(BOOL)bForever
{
    if (m_nCheck == nAction) // if action is same , return
        return;
    m_nCheck = nAction;
    int nStep = 0;
    int nStartAct = 0;
    
    switch (m_nCheck) {
        case ACTION_STR:
        case ACTION_UP:
        case ACTION_DOWN:
            nStep = 12;
            nStartAct = 1;
            break;
        case ACTION_DIE:
            nStep = 6;
            nStartAct = 0;
            break;
        case ACTION_FIGHT1:
            nStep = 12;
            nStartAct = 2;
            break;
        case ACTION_FIGHT2:
            nStep = 12;
            nStartAct = 3;
            break;
        case ACTION_FIGHT3:
            nStep = 12;
            nStartAct = 4;
            break;
        case ACTION_FIGHT4:
            nStep = 12;
            nStartAct = 5;
            break;
        case ACTION_FIGHT5:
            nStep = 12;
            nStartAct = 6;
            break;
        case ACTION_FIGHT6:
            nStep = 12;
            nStartAct = 7;
            break;
        case ACTION_READY:
            nStep = 1;
            nStartAct = 2;
            break;
            
        default:
            break;
    }

    if (bForever)
    {
        CCAnimation *animation = [CCAnimation animation];
        for(int i = 0; i < nStep; i++) {
            [animation addFrameWithTexture:[[MainMenuLayer ShareInstance] getTexture:1 order:m_nType]  rect:CGRectMake(i * 80, nStartAct * 80 + 2, 80, 80)];
        }
        CCAnimate *animateAction = [CCAnimate actionWithDuration: ACTION_SPEED animation: animation restoreOriginalFrame: NO];
        CCRepeatForever *repeatAction = [CCRepeatForever actionWithAction:animateAction];
        [self runAction: repeatAction];
    }
    else
    {
        CGFloat fTime = 0.05;
        if (nAction == ACTION_DIE)
            fTime = 0.1;
        CCAnimation *animation = [[CCAnimation alloc] initWithName:@"enemy_idle" delay:fTime];
        for(int i = 0; i < nStep; i++) {
            [animation addFrameWithTexture:[[MainMenuLayer ShareInstance] getTexture:1 order:m_nType] rect:CGRectMake(i * 80, nStartAct * 80 + 2, 80, 80)];
        }
        CCAnimate   *actionEffect = [CCAnimate actionWithAnimation:animation restoreOriginalFrame: NO];
        CCRepeat    *repeatAction = [CCRepeat actionWithAction:actionEffect times:1];
        [self runAction: repeatAction];
    }
}

-(void) hittedArrow:(Arrow*)arrow posX:(CGFloat)nX posY:(CGFloat)nY
{
    arrow.position = ccp(nX, nY);
    [self addChild:arrow z:-1];
    [m_aryArrow addObject:arrow];
    //[arrow release];
}

-(void) moveCharacter
{
    CGPoint pos = self.position;
    CGPoint ptPos = [self tilePosFromLocation:ccp(pos.x + self.contentSize.width / 2, pos.y)];
    int result = [self isBlock:ptPos];
    
    if (result == DIR_NOTHING) // nothing block
    {
        //[self setAnimation:ACTION_DOWN repeat:YES];
        [self setAnimation:ACTION_STR repeat:YES];
        CGPoint ptPos_down = ccpAdd(ptPos, ccp(0, 1));
        int result_down = [self isBlock:ptPos_down];
        
        pos.y -= 2;
        
        if ((result_down == DIR_DOWN) || (result_down == DIR_DOWN_SEAT))
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
        }

    }
    
    if ((result == DIR_UP) || (result == DIR_UP_SEAT)) // up direction
    {
        //[self setAnimation:ACTION_UP repeat:YES];
        [self setAnimation:ACTION_STR repeat:YES];
        pos.x += ENEMY1_SPEED_X * m_nDir;
        pos.y += ENEMY1_SPEED_Y;
    }

    if ((result == DIR_LEFT_TO_RIGHT) || (result == DIR_LTR_SEAT)) // up direction (left to right)
    {
        if (m_nDir == 1)
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
            pos.y += ENEMY1_SPEED_Y;
        }
    }
    if ((result == DIR_RIGHT_TO_LEFT) || (result == DIR_RTL_SEAT)) // up direction (right to left)
    {
        if (m_nDir == -1)
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
            pos.y += ENEMY1_SPEED_Y;
        }
    }
    if (result == DIR_LR_STR) // if enemy goes from left to right, change direction, if right to left, no change
    {
        if (m_nDir == 1)
        {
            m_nDir = -1;
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 180;
        }
        else
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
        }
    }
    if (result == DIR_RL_STR) // if enemy goes from right to left, change direction, if left to right, no change
    {
        if (m_nDir == -1)
        {
            m_nDir = 1;
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 0;
        }
        else
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
        }
    }
    
    if (result == DIR_LR_RAND) // if enemy goes from left to right, some enemy will go straight, some will change direction
    {
        if (m_nDir == 1)
        {
            if (m_bRand == NO)
            {
                m_nRandTemp = [GameViewLayer getRandomNumberBetweenMin:0 andMax:2];
                m_bRand = YES;
            }
            if (m_nRandTemp == 0)
            {
                m_nDir = -1;
                pos.x += ENEMY1_SPEED_X * m_nDir;
                self.flipX = 180;
            }
            else
            {
                pos.x += ENEMY1_SPEED_X * m_nDir;
                self.flipX = 0;
            }
        }
        else
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 180;
        }
    }
    else
    {
        m_bRand = NO;
    }
    if (result == DIR_RL_RAND) // if enemy goes from right to left, some enemy will go straight, some will change direction
    {
        if (m_nDir == -1)
        {
            if (m_bRand == NO)
            {
                m_nRandTemp = [GameViewLayer getRandomNumberBetweenMin:0 andMax:2];
                m_bRand = YES;
            }
            if (m_nRandTemp == 0)
            {
                m_nDir = 1;
                pos.x += ENEMY1_SPEED_X * m_nDir;
                self.flipX = 0;
            }
            else
            {
                pos.x += ENEMY1_SPEED_X * m_nDir;
                self.flipX = 180;
            }
        }
        else
        {
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 0;
        }
    }
    else
    {
        m_bRand = NO;
    }

    if ((result == DIR_DOWN) || (result == DIR_DOWN_SEAT))// down direction
    {
        //[self setAnimation:ACTION_DOWN repeat:YES];
        [self setAnimation:ACTION_STR repeat:YES];
        CGPoint ptPos_down = ccpAdd(ptPos, ccp(m_nDir, 1));
        int result_down = [self isBlock:ptPos_down];
        
        pos.x += ENEMY1_SPEED_X * m_nDir;
        
        if ((result_down == DIR_DOWN) || (result_down == DIR_DOWN_SEAT))
        {
            pos.y -= ENEMY1_SPEED_Y;
        }
    }
    
    if ((result == DIR_STRAIGHT) || (result == DIR_STR_SEAT))// straight direction
    {
        [self setAnimation:ACTION_STR repeat:YES];

        CGPoint ptPos_next = ccpAdd(ptPos, ccp(m_nDir, -1));
        int result_next = [self isBlock:ptPos_next];
        
        pos.x += ENEMY1_SPEED_X * m_nDir;

        if ((result_next == DIR_LEFT_TO_RIGHT) || (result_next == DIR_LTR_SEAT)) // left to right block
        {
            if (m_nDir == 1)
            {
                pos.y += ENEMY1_SPEED_Y;
            }
        }
        if ((result_next == DIR_RIGHT_TO_LEFT) || (result_next == DIR_RTL_SEAT)) // right to left block
        {
            if (m_nDir == -1)
            {
                pos.y += ENEMY1_SPEED_Y;
            }
        }
    }

    if (result == DIR_END_LR) // end of left to right stair
    {
        if (m_nDir == 1)
        {
            m_nDir = -1;
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 180;

        }
    }
    
    if (result == DIR_END_RL) // end of right to left stair
    {
        if (m_nDir == -1)
        {
            m_nDir = 1;
            pos.x += ENEMY1_SPEED_X * m_nDir;
            self.flipX = 0;
        }
    }
    
    if (result == DIR_STR_RAND) // enemy will define random direction
    {
        if (m_bRand == NO)
        {
            m_nRandTemp = [GameViewLayer getRandomNumberBetweenMin:0 andMax:2];
            m_bRand = YES;
            
            if (m_nRandTemp == 0)
            {
                m_nDir = 1;
                self.flipX = 0;
            }
            else
            {
                m_nDir = -1;
                self.flipX = 180;
            }
        }
        pos.x += ENEMY1_SPEED_X * m_nDir;
    }
    else
    {
        m_bRand = NO;
    }

    if (result == DIR_GOLD) // arrived to gold
    {
        [GameViewLayer ShareInstance].m_nGold --;
        [[GameViewLayer ShareInstance] updateGoldNumber];
        
        m_bDie = YES;
        [self removeAllEnemyInfo];
        [self removeFromParentAndCleanup:YES];
        return;
    }
    
    self.position = pos;
}

-(void) selectFighter
{
    if (m_bFightFlag == YES) return;
    
    if ([[GameViewLayer ShareInstance].m_aryFighter count] == 0) return;
    
    int nIndex = 0;
    int nAryFighter[[[GameViewLayer ShareInstance].m_aryFighter count]];
    CGFloat nAryGapX[[[GameViewLayer ShareInstance].m_aryFighter count]];
    CGFloat nAryGapY[[[GameViewLayer ShareInstance].m_aryFighter count]];
    
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryFighter count]; i ++)
    {
        nAryFighter[i] = 0;
        nAryGapX[i] = 0;
        nAryGapY[i] = 0;
    }
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryFighter count]; i ++)
    {
        Fighter *fighter = [[GameViewLayer ShareInstance].m_aryFighter objectAtIndex:i];
        
        if ((fighter.m_bDie == YES) || (fighter.m_bFightFlag == YES))
            continue;
        
        CGFloat nFighter_X = fighter.position.x;
        CGFloat nFighter_Y = fighter.position.y;
        CGFloat nSelf_X = self.position.x;
        CGFloat nSelf_Y = self.position.y;
        
        // ------------------- check enemy direction and position ---------------- //
        CGPoint pos = self.position;
        CGPoint ptPos = [self tilePosFromLocation:ccp(pos.x + self.contentSize.width / 2, pos.y)];
        int result = [self isBlock:ptPos];
        if ((result == DIR_UP) && (self.position.y > fighter.position.y))
        {
            continue;
        }
        if ((result == DIR_DOWN) && (self.position.y < fighter.position.y))
        {
            continue;
        }
        
        CGPoint ptPos_center = [fighter tilePosFromLocation:ccp(fighter.position.x, fighter.position.y - OFFSET)];
        int result_center = [fighter isBlock:ptPos_center];
        
        CGPoint ptPos_left_up = ccpAdd(ptPos_center, ccp(-1, -1));
        int result_left_up = [fighter isBlock:ptPos_left_up];
        
        CGPoint ptPos_left_down = ccpAdd(ptPos_center, ccp(-1, 1));
        int result_left_down = [fighter isBlock:ptPos_left_down];
        
        CGPoint ptPos_right_up = ccpAdd(ptPos_center, ccp(1, -1));
        int result_right_up = [fighter isBlock:ptPos_right_up];
        
        CGPoint ptPos_right_down = ccpAdd(ptPos_center, ccp(1, 1));
        int result_right_down = [fighter isBlock:ptPos_right_down];
        
        
        if ((self.m_nDir == 1) && (nFighter_X - nSelf_X > 15.0f) && (nFighter_X - nSelf_X < 60.0f))
        {
            if (((result_center == DIR_UP) || (result_center == DIR_RIGHT_TO_LEFT)) && (result_left_up == DIR_UP))
            {
                continue;
            }
            if ((result_center == DIR_DOWN) && (result_left_up == DIR_DOWN) && (result_right_down == DIR_DOWN))
            {
                if (self.position.y < fighter.position.y)
                    continue;
            }
            nAryFighter[nIndex] = i;
            nAryGapX[nIndex] = ABS(nSelf_X - nFighter_X);
            nAryGapY[nIndex] = ABS(nSelf_Y - nFighter_Y);
            nIndex ++;
        }
        if ((self.m_nDir == -1) && (nSelf_X - nFighter_X > 15.0f) && (nSelf_X - nFighter_X < 60.0f))
        {
            if (((result_center == DIR_UP) || (result_center == DIR_LEFT_TO_RIGHT)) && (result_right_up == DIR_UP))
            {
                continue;
            }
            if ((result_center == DIR_DOWN) && (result_right_up == DIR_DOWN) && (result_left_down == DIR_DOWN))
            {
                if (self.position.y < fighter.position.y)
                    continue;
            }
            
            nAryFighter[nIndex] = i;
            nAryGapX[nIndex] = ABS(nSelf_X - nFighter_X);
            nAryGapY[nIndex] = ABS(nSelf_Y - nFighter_Y);
            nIndex ++;
        }
    }
    
    if (nIndex == 0)
    {
        m_nFighter = -1;
        return;
    }
    
    m_nFighter = -1;
    
    for (int i = 0; i < nIndex; i ++)
    {
        if ((nAryGapX[i] < 32.0f) && (nAryGapX[i] > 15.0f) && (nAryGapY[i] < 20.0f))
        {
            m_nFighter = nAryFighter[i];
            m_nArcher = -1;
            
            [self unscheduleAllSelectors];
            Fighter *fighter = [[GameViewLayer ShareInstance].m_aryFighter objectAtIndex:m_nFighter];
            
            if (self.m_nDir == -1)
                fighter.flipX = 180;
            else
                fighter.flipX = 0;
            
            m_bFightFlag = YES;
            fighter.m_bFightFlag = YES;
            [fighter startFight];
            
            [self stopAllActions];
            [self startFight];
            return;
        }
    }
}

-(void) selectArcher
{
    if (m_bFightFlag == YES) return;
    
    if ([[GameViewLayer ShareInstance].m_aryArcher count] == 0) return;
    
    int nIndex = 0;
    int nAryArcher[[[GameViewLayer ShareInstance].m_aryArcher count]];
    CGFloat nAryGapX[[[GameViewLayer ShareInstance].m_aryArcher count]];
    CGFloat nAryGapY[[[GameViewLayer ShareInstance].m_aryArcher count]];
    
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryArcher count]; i ++)
    {
        nAryArcher[i] = 0;
        nAryGapX[i] = 0;
        nAryGapY[i] = 0;
    }
    for (int i = 0; i < [[GameViewLayer ShareInstance].m_aryArcher count]; i ++)
    {
        Archer *archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:i];
        
        if ((archer.m_bDie == YES) || (archer.m_bFightFlag == YES))
            continue;
        
        CGFloat nArcher_X = archer.position.x;
        CGFloat nArcher_Y = archer.position.y;
        CGFloat nSelf_X = self.position.x;
        CGFloat nSelf_Y = self.position.y;
        
        // ------------------- check enemy direction and position ---------------- //
        CGPoint pos = self.position;
        CGPoint ptPos = [self tilePosFromLocation:ccp(pos.x + self.contentSize.width / 2, pos.y)];
        int result = [self isBlock:ptPos];
        if ((result == DIR_UP) && (self.position.y > archer.position.y))
        {
            continue;
        }
        if ((result == DIR_DOWN) && (self.position.y < archer.position.y))
        {
            continue;
        }
        
        CGPoint ptPos_center = [archer tilePosFromLocation:ccp(archer.position.x, archer.position.y - OFFSET)];
        int result_center = [archer isBlock:ptPos_center];
        
        CGPoint ptPos_left_up = ccpAdd(ptPos_center, ccp(-1, -1));
        int result_left_up = [archer isBlock:ptPos_left_up];
        
        CGPoint ptPos_left_down = ccpAdd(ptPos_center, ccp(-1, 1));
        int result_left_down = [archer isBlock:ptPos_left_down];
        
        CGPoint ptPos_right_up = ccpAdd(ptPos_center, ccp(1, -1));
        int result_right_up = [archer isBlock:ptPos_right_up];
        
        CGPoint ptPos_right_down = ccpAdd(ptPos_center, ccp(1, 1));
        int result_right_down = [archer isBlock:ptPos_right_down];
        
        
        if ((self.m_nDir == 1) && (nArcher_X - nSelf_X > 15.0f) && (nArcher_X - nSelf_X < 60.0f))
        {
            if (((result_center == DIR_UP) || (result_center == DIR_RIGHT_TO_LEFT)) && (result_left_up == DIR_UP))
            {
                continue;
            }
            if ((result_center == DIR_DOWN) && (result_left_up == DIR_DOWN) && (result_right_down == DIR_DOWN))
            {
                if (self.position.y < archer.position.y)
                    continue;
            }
            nAryArcher[nIndex] = i;
            nAryGapX[nIndex] = ABS(nSelf_X - nArcher_X);
            nAryGapY[nIndex] = ABS(nSelf_Y - nArcher_Y);
            nIndex ++;
        }
        if ((self.m_nDir == -1) && (nSelf_X - nArcher_X > 15.0f) && (nSelf_X - nArcher_X < 60.0f))
        {
            if (((result_center == DIR_UP) || (result_center == DIR_LEFT_TO_RIGHT)) && (result_right_up == DIR_UP))
            {
                continue;
            }
            if ((result_center == DIR_DOWN) && (result_right_up == DIR_DOWN) && (result_left_down == DIR_DOWN))
            {
                if (self.position.y < archer.position.y)
                    continue;
            }
            
            nAryArcher[nIndex] = i;
            nAryGapX[nIndex] = ABS(nSelf_X - nArcher_X);
            nAryGapY[nIndex] = ABS(nSelf_Y - nArcher_Y);
            nIndex ++;
        }
    }
    
    if (nIndex == 0)
    {
        m_nArcher = -1;
        return;
    }
    
    m_nArcher = -1;
    
    for (int i = 0; i < nIndex; i ++)
    {
        if ((nAryGapX[i] < 32.0f) && (nAryGapX[i] > 15.0f) && (nAryGapY[i] < 20.0f))
        {
            m_nArcher = nAryArcher[i];
            m_nFighter = -1;
            
            [self unscheduleAllSelectors];
            Archer *archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:m_nArcher];
            
            m_bFightFlag = YES;
            archer.m_bFightFlag = YES;

            [self stopAllActions];
            [self startFight];
            return;
        }
    }
}

-(int) isBlock: (CGPoint) tilePos
{
	int nIsBlock = 0;
    if(tilePos.x < g_tmxMap.mapSize.width && tilePos.x >= 0 && tilePos.y >= 0 && tilePos.y < g_tmxMap.mapSize.height)
    {
        int tileGID = [[g_tmxMap layerNamed:@"Stair_Layer"] tileGIDAt:tilePos];
        if(tileGID != 0)
        {
            NSDictionary *properties = [g_tmxMap propertiesForGID:tileGID];
            if(properties)
            {
                NSString *isBlock = [properties valueForKey:@"Stair"];
                nIsBlock = [isBlock intValue];
                isBlock = nil;
            }
        }
    }
	return nIsBlock;
}

-(CGPoint) tilePosFromLocation:(CGPoint) ptPos
{
	ptPos.x = (int)((ptPos.x - OFFSET) / TILE_WIDTH);
	ptPos.y = (int)((ptPos.y - OFFSET) / TILE_HEIGHT);
    ptPos.y = g_tmxMap.mapSize.height - ptPos.y - 1;
	return ptPos;
}

-(void) cleanupCharater {
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
}

-(void) dealloc
{
    [self cleanupCharater];
	[super dealloc];
}


@end
