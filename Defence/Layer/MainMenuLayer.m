//
//  MainMenuLayer.m
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainMenuLayer.h"
#import "GameViewLayer.h"
#import "SettingViewLayer.h"
#import "PurchaseLayer.h"
#import "ConstValue.h"
#import "GameSaveInfo.h"
#import "GameSaver.h"
#import "SNAdsManager.h"
#import "SettingsManager.h"
#import "RootViewController.h"
#import "MKStoreManager.h"

@implementation MainMenuLayer

@synthesize m_txtEnemy1;
@synthesize m_txtEnemy2;
@synthesize m_txtEnemy3;
@synthesize m_txtEnemy4;
@synthesize m_txtEnemy5;
@synthesize m_txtEnemy6;
@synthesize m_txtSolder1;
@synthesize m_txtSolder2;
@synthesize m_txtSolder3;

static MainMenuLayer* g_cMainMenuLayer = nil;
static CGSize size;// = [[CCDirector sharedDirector] winSize];
+(id) scene
{
	CCScene *scene = [CCScene node];
	MainMenuLayer *layer = [MainMenuLayer ShareInstance];
	[scene addChild: layer];
	return scene;
}

+(MainMenuLayer*) ShareInstance
{
    if (!g_cMainMenuLayer)
    {
        g_cMainMenuLayer = [[MainMenuLayer alloc] init];
    }
    return g_cMainMenuLayer;
}

-(id) init
{
	if( (self = [super init]) ) {
        size = [[CCDirector sharedDirector] winSize];
        m_spLoading = [CCSprite spriteWithFile:@"loading.png"];
        m_spLoading.position = ccp(DEVICE_WIDTH / 2 + 10, DEVICE_HEIGHT / 2);
        [self addChild:m_spLoading];
        
        [self loadingAnimation];
        [self schedule: @selector(enterGame:) interval:1];
    }
    return self;
}

- (void)addBandAid{
    CGSize size = [[CCDirector sharedDirector] winSize];
    CCSprite *bandAidSprite = [CCSprite spriteWithFile:@"menu_bandaid.png"];
    bandAidSprite.scale = DEVICE_SCALE;
    // bandAidSprite.anchorPoint = ccp(0, 0);
    if ([SettingsManager sharedManager].isIPhone5)
        bandAidSprite.position = ccp(60 + bandAidSprite.boundingBox.size.width/2, size.height - bandAidSprite.boundingBox.size.height/2 - 20);
    else
        bandAidSprite.position = ccp(20 + bandAidSprite.boundingBox.size.width/2, size.height - bandAidSprite.boundingBox.size.height/2 - 20);
    [self addChild:bandAidSprite z:11];
}

- (void)addMoreGamesButton{
    CCMenuItemImage *moreGamesItem = [CCMenuItemImage itemFromNormalImage:@"free-games-gray.png" selectedImage:@"free-games-gray.png" target:self selector:@selector(moreGamesButtonTapped)];
    moreGamesItem.scale = DEVICE_SCALE;
    moreGamesItem.position = ccp(size.width - moreGamesItem.boundingBox.size.width/2, moreGamesItem.boundingBox.size.height/2);
    CCMenu *moreGamesMenu = [CCMenu menuWithItems:moreGamesItem, nil];
    moreGamesMenu.position = ccp(0, 0);
    [self addChild:moreGamesMenu z:11];
}

- (void)addRestoreAndRemoveAdButtons{
#ifdef FreeApp
    if (![SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        CCMenuItemImage *removeAdsButton = [CCMenuItemImage itemFromNormalImage:@"brownremoveads.png" selectedImage:@"brownremoveads.png" disabledImage:@"brownremoveads.png" target:self selector:@selector(removeAdsButtonTapped:)];
        removeAdsButton.position = ccp(200, 50);
        removeAdsButton.scale = DEVICE_SCALE;
        removeAdsButton.tag = 21;
        
        CCMenuItemImage *restoreButton = [CCMenuItemImage itemFromNormalImage:@"brownrestore.png" selectedImage:@"brownrestore.png" disabledImage:@"brownrestore.png" target:self selector:@selector(retoreButtonTapped:)];
        restoreButton.position = ccp(360, 50);
        restoreButton.scale = DEVICE_SCALE;
        restoreButton.tag = 20;
        
        CCMenu* newMenu = [CCMenu menuWithItems:removeAdsButton, restoreButton, nil];
        newMenu.position = ccp(0,0);
        newMenu.tag = 23;
        [self addChild:newMenu z:1];
    }
    
#endif
}
- (void)moreGamesButtonTapped{
    [[SNAdsManager sharedManager] giveMeMoreAppsAd];
}


-(void) enterGame: (ccTime) delta
{
    [self unscheduleAllSelectors];
    [m_spLoading removeFromParentAndCleanup:YES];
    m_spLoading = nil;
    
    // ------------------------  end loading -------------------------- //
    backSprite = [CCSprite spriteWithFile:@"menu.png"];
    backSprite.scale = DEVICE_SCALE;
    if ([SettingsManager sharedManager].isIPhone5) {
        backSprite.position = ccp(DEVICE_WIDTH/2 +40, DEVICE_HEIGHT/2);
    }else{
        backSprite.anchorPoint = ccp(0, 0);
    }
    
    [self addChild:backSprite z:LAYER_GAME_BACK];
    
    m_fMenuStart_X = DEVICE_WIDTH + 220;
    m_fMenu_X = m_fMenuStart_X;
    m_fLoad_X = m_fMenuStart_X;
    m_fSetting_X = m_fMenuStart_X;
    m_fHighscore_X = m_fMenuStart_X;
    m_fHelp_X = m_fMenuStart_X;
    m_fNormal_X = m_fMenuStart_X;
    m_fSurvival_X = m_fMenuStart_X;
    
    m_bStop = NO;
    
    [self attachMenu];
    [self playBackSound:@"menu_music"];
    [self addBandAid];
    [self addMoreGamesButton];
    [self addRestoreAndRemoveAdButtons];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
}


-(void) attachMenu
{
    item_new = [CCMenuItemImage itemFromNormalImage:@"btn_01.PNG" selectedImage:@"btn_01_down.PNG" target:self selector:@selector(clickNewGame:)];
    item_load = [CCMenuItemImage itemFromNormalImage:@"btn_02.PNG" selectedImage:@"btn_02_down.PNG" target:self selector:@selector(clickLoadGame:)];
    item_highscore = [CCMenuItemImage itemFromNormalImage:@"btn_03.PNG" selectedImage:@"btn_03_down.PNG" target:self selector:@selector(clickHighScore:)];
    item_setting = [CCMenuItemImage itemFromNormalImage:@"btn_04.PNG" selectedImage:@"btn_04_down.PNG" target:self selector:@selector(clickSetting:)];
    item_help = [CCMenuItemImage itemFromNormalImage:@"btn_05.PNG" selectedImage:@"btn_05_down.PNG" target:self selector:@selector(clickHelp:)];
    item_normal = [CCMenuItemImage itemFromNormalImage:@"btn_01_01.PNG" selectedImage:@"btn_01_01_down.PNG" target:self selector:@selector(clickNormal:)];
    item_survival = [CCMenuItemImage itemFromNormalImage:@"btn_01_02.PNG" selectedImage:@"btn_01_02_down.PNG" target:self selector:@selector(clickSurvival:)];
    item_back = [CCMenuItemImage itemFromNormalImage:@"btn_back.PNG" selectedImage:@"btn_back_down.PNG" target:self selector:@selector(clickBack:)];
    
    mainMenu = [CCMenu menuWithItems: item_new, item_load, item_setting, item_highscore, item_help, item_normal, item_survival, item_back, nil];
    //mainMenu.position = CGPointMake(44, 0);
    mainMenu.position = [SettingsManager sharedManager].isIPhone5 ? CGPointMake(54, 0) : CGPointZero;
    
    item_new.scale = DEVICE_SCALE;
    item_load.scale = DEVICE_SCALE;
    item_highscore.scale = DEVICE_SCALE;
    item_setting.scale = DEVICE_SCALE;
    item_help.scale = DEVICE_SCALE;
    item_normal.scale = DEVICE_SCALE;
    item_survival.scale = DEVICE_SCALE;
    item_back.scale = DEVICE_SCALE;
    
    item_new.position = ccp(m_fMenu_X, MENU_POS_Y);
    item_load.position = ccp(m_fLoad_X, MENU_POS_Y - 40);
    item_highscore.position = ccp(m_fHighscore_X, MENU_POS_Y - 80);
    item_setting.position = ccp(m_fSetting_X, MENU_POS_Y - 120);
    item_help.position = ccp(m_fHelp_X, MENU_POS_Y - 160);
    item_normal.position = ccp(m_fNormal_X, MENU_POS_Y - 20);
    item_survival.position = ccp(m_fSurvival_X, MENU_POS_Y - 80);
    item_back.position = ccp(MENU_POS_X + 40, MENU_POS_Y - 160);
    
    item_back.visible = NO;
    [self addChild: mainMenu z:LAYER_ENVIRONMENT];
    
    [self schedule: @selector(menuChange:) interval:0.001];
    item_help.visible = NO;
}

-(void) clickNewGame: (id) sender
{
    if (m_bStop == YES)
        return;
    [self schedule: @selector(menuNewGame:) interval:0.001];
}

-(void) clickNormal: (id) sender
{
    if (m_bStop == YES)
        return;
    
    GameSaver *gameSaver = [[GameSaver alloc] init];
    GameSaveInfo *gameInfo = [[GameSaveInfo alloc] init];
    [gameSaver readGameInfo:gameInfo];
    
    [GameViewLayer setInfo:1 money:gameInfo.m_nNewMoney score:0];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[GameViewLayer scene]]];    
    
    [gameInfo release];
    [gameSaver release];

}

-(void) clickSurvival: (id) sender
{
    if (m_bStop == YES)
        return;

    GameSaver *gameSaver = [[GameSaver alloc] init];
    GameSaveInfo *gameInfo = [[GameSaveInfo alloc] init];
    [gameSaver readGameInfo:gameInfo];

    [GameViewLayer setInfo:31 money:gameInfo.m_nNewMoney score:0];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[GameViewLayer scene]]];
    
    [gameInfo release];
    [gameSaver release];
}

-(void) clickLoadGame: (id) sender
{
    if (m_bStop == YES)
        return;
    
    GameSaver *gameSaver = [[GameSaver alloc] init];
    GameSaveInfo *gameInfo = [[GameSaveInfo alloc] init];
    [gameSaver readGameInfo:gameInfo];
    
    int nMoney = gameInfo.m_nMoney + gameInfo.m_nNewMoney;
    if (gameInfo.m_nLevel > 1)
    {
        [GameViewLayer setInfo:gameInfo.m_nLevel money:nMoney score:gameInfo.m_nScore];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[GameViewLayer scene]]];

    gameInfo.m_nMoney = nMoney;
    gameInfo.m_nNewMoney = 0;
    [gameSaver writeGameInfo:gameInfo];

    [gameInfo release];
    [gameSaver release];
}

-(void) clickSetting: (id) sender
{
    if (m_bStop == YES)
        return;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[SettingViewLayer scene]]];
}

-(void) clickHighScore: (id) sender
{
    if (m_bStop == YES)
        return;
    [appDelegate showLeaderboard];
}

-(void) clickHelp: (id) sender
{
    if (m_bStop == YES)
        return;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.7 scene:[PurchaseLayer scene]]];
}

-(void) menuNewGame: (ccTime) delta
{
    m_bStop = YES;
    BOOL bMenu1 = NO;
    BOOL bMenu2 = NO;
    BOOL bMenu3 = NO;
    BOOL bMenu4 = NO;
    BOOL bMenu5 = NO;
    
    if (m_fMenu_X < DEVICE_WIDTH + 220)
    {
        m_fMenu_X += 40;
    }
    else
    {
        bMenu1 = YES;
    }
    
    if ((m_fLoad_X < DEVICE_WIDTH + 220) && (m_fMenu_X > DEVICE_WIDTH - 100))
    {
        m_fLoad_X += 40;
    }
    else
    {
        bMenu2 = YES;
    }
    if ((m_fHighscore_X < DEVICE_WIDTH + 220) && (m_fLoad_X > DEVICE_WIDTH - 100))
    {
        m_fHighscore_X += 40;
    }
    else
    {
        bMenu3 = YES;
    }
    if ((m_fSetting_X < DEVICE_WIDTH + 220) && (m_fHighscore_X > DEVICE_WIDTH - 100))
    {
        m_fSetting_X += 40;
    }
    else
    {
        bMenu4 = YES;
    }
    if ((m_fHelp_X < DEVICE_WIDTH + 220) && (m_fSetting_X > DEVICE_WIDTH - 100))
    {
        m_fHelp_X += 40;
    }
    else
    {
        bMenu5 = YES;
    }
    
    
    item_new.position = ccp(m_fMenu_X, item_new.position.y);
    item_load.position = ccp(m_fLoad_X, item_load.position.y);
    item_highscore.position = ccp(m_fHighscore_X, item_highscore.position.y);
    item_setting.position = ccp(m_fSetting_X, item_setting.position.y);
    item_help.position = ccp(m_fHelp_X, item_help.position.y);
    
    if ((bMenu1 == YES) && (bMenu2 == YES) && (bMenu3 == YES) && (bMenu4 == YES) && (bMenu5 == YES))
    {
        [self unscheduleAllSelectors];
        [self schedule:@selector(newGame:) interval:0.001];
    }
    item_help.visible = NO;
}

-(void) newGame: (ccTime) delta
{
    BOOL bMenu1 = NO;
    BOOL bMenu2 = NO;
    
    if (m_fNormal_X > MENU_POS_X)
    {
        m_fNormal_X -= 40;
    }
    else
    {
        bMenu1 = YES;
    }
    
    if ((m_fSurvival_X > MENU_POS_X) && (m_fNormal_X < DEVICE_WIDTH - 100))
    {
        m_fSurvival_X -= 40;
    }
    else
    {
        bMenu2 = YES;
    }

    item_normal.position = ccp(m_fNormal_X, item_new.position.y);
    item_survival.position = ccp(m_fSurvival_X, item_load.position.y);
    
    if ((bMenu1 == YES) && (bMenu2 == YES))
    {
        item_back.visible = YES;
        [self unscheduleAllSelectors];
        m_bStop = NO;
    }
}

-(void) clickBack: (id) sender
{
    if (m_bStop == YES)
        return;
    [self schedule:@selector(returnMenu:) interval:0.001];
}

-(void) returnMenu: (ccTime) delta
{
    m_bStop = YES;
    BOOL bMenu1 = NO;
    BOOL bMenu2 = NO;
    
    if (m_fNormal_X < DEVICE_WIDTH + 220)
    {
        m_fNormal_X += 40;
    }
    else
    {
        bMenu1 = YES;
    }
    
    if ((m_fSurvival_X < DEVICE_WIDTH + 220) && (m_fNormal_X > DEVICE_WIDTH - 100))
    {
        m_fSurvival_X += 40;
    }
    else
    {
        bMenu2 = YES;
    }
   
    item_normal.position = ccp(m_fNormal_X, item_normal.position.y);
    item_survival.position = ccp(m_fSurvival_X, item_survival.position.y);
    
    if ((bMenu1 == YES) && (bMenu2 == YES))
    {
        item_back.visible = NO;
        [self unscheduleAllSelectors];
        [self schedule:@selector(menuChange:) interval:0.001];
    }
}

-(void) menuChange: (ccTime) delta
{
    m_bStop = YES;
    BOOL bMenu1 = NO;
    BOOL bMenu2 = NO;
    BOOL bMenu3 = NO;
    BOOL bMenu4 = NO;
    BOOL bMenu5 = NO;
    
    if (m_fMenu_X > MENU_POS_X)
    {
        m_fMenu_X -= 40;
    }
    else
    {
        bMenu1 = YES;
    }
    
    if ((m_fLoad_X > MENU_POS_X) && (m_fMenu_X < DEVICE_WIDTH - 100))
    {
        m_fLoad_X -= 40;
    }
    else
    {
        bMenu2 = YES;
    }
    if ((m_fHighscore_X > MENU_POS_X) && (m_fLoad_X < DEVICE_WIDTH - 100))
    {
        m_fHighscore_X -= 40;
    }
    else
    {
        bMenu3 = YES;
    }
    if ((m_fSetting_X > MENU_POS_X) && (m_fHighscore_X < DEVICE_WIDTH - 100))
    {
        m_fSetting_X -= 40;
    }
    else
    {
        bMenu4 = YES;
    }
    if ((m_fHelp_X > MENU_POS_X) && (m_fSetting_X < DEVICE_WIDTH - 100))
    {
        m_fHelp_X -= 40;
    }
    else
    {
        bMenu5 = YES;
    }
    
    
    item_new.position = ccp(m_fMenu_X, item_new.position.y);
    item_load.position = ccp(m_fLoad_X, item_load.position.y);
    item_highscore.position = ccp(m_fHighscore_X, item_highscore.position.y);
    item_setting.position = ccp(m_fSetting_X, item_setting.position.y);
    item_help.position = ccp(m_fHelp_X, item_help.position.y);
    
    if ((bMenu1 == YES) && (bMenu2 == YES) && (bMenu3 == YES) && (bMenu4 == YES) && (bMenu5 == YES))
    {
        [self unscheduleAllSelectors];
        m_bStop = NO;
    }
}

- (void)playBackSound:(NSString*)filename
{
    if ([SettingViewLayer ShareInstance].m_bBackMusic == NO)
        return;
    path = [[NSBundle mainBundle] pathForResource:filename ofType:@"mp3"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) 
    {    
        back_player = [[AVAudioPlayer alloc] initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:&error];
        
        back_player.volume = 0.5f;
        
        [back_player prepareToPlay];
        
        [back_player setNumberOfLoops:-1];
        
        [back_player play];
    }
}

- (void)playEffectSound:(NSString*)filename nX:(int)nX nY:(int)nY
{
    if ([SettingViewLayer ShareInstance].m_bEffectMusic == NO)
        return;
    // ---------------- if character show on screen ------------------ //
    
    /*
    if ((nX / 2 + g_tmxMap.position.x < -1 * OFFSET) || (nX / 2 + g_tmxMap.position.x > DEVICE_WIDTH + OFFSET) ||
        (nY / 2 + g_tmxMap.position.y < -1 * OFFSET) || (nY / 2 + g_tmxMap.position.y - MAP_START_Y > DEVICE_HEIGHT + OFFSET))
    {
        return;
    }
     */

    // --------------------------------------------------------------- //
    
    path = [[NSBundle mainBundle] pathForResource:filename ofType:@"mp3"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) 
    {    
        AVAudioPlayer *effect_player = [[AVAudioPlayer alloc] initWithContentsOfURL:
                                        [NSURL fileURLWithPath:path] error:&error];
        
        effect_player.volume = 0.8f;
        
        effect_player.delegate = self;
        
        [effect_player prepareToPlay];
        
        [effect_player setNumberOfLoops:0];
        
        [effect_player play];
    }
    
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player release];
}

- (void)removeBackSound
{
    if (back_player.isPlaying == YES)
        [back_player stop];
    [back_player release];
    back_player = nil;
}

- (void)resumeBackSound
{
    if (back_player.isPlaying == NO)
        [back_player play];
}

- (void)pauseBackSound
{
    if (back_player.isPlaying == YES)
        [back_player stop];
}

-(void) loadingAnimation
{
    // ------------ loading animation ----------- //
/*
    m_txtSolder1 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier1_act.png"]] retain];
    m_txtSolder2 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier2_act.png"]] retain];
    m_txtSolder3 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier3_act.png"]] retain];
    m_txtEnemy1 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy1_act.png"]] retain];
    m_txtEnemy2 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy2_act.png"]] retain];
    m_txtEnemy3 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy3_act.png"]] retain];
    m_txtEnemy4 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy4_act.png"]] retain];
    m_txtEnemy5 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy5_act.png"]] retain];
    m_txtEnemy6 = [[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy6_act.png"]] retain];
 */
    self.m_txtSolder1 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier1_act.png"]];
    self.m_txtSolder2 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier2_act.png"]];
    self.m_txtSolder3 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"soldier3_act.png"]];
    self.m_txtEnemy1 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy1_act.png"]];
    self.m_txtEnemy2 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy2_act.png"]];
    self.m_txtEnemy3 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy3_act.png"]];
    self.m_txtEnemy4 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy4_act.png"]];
    self.m_txtEnemy5 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy5_act.png"]];
    self.m_txtEnemy6 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"enemy6_act.png"]];

}

-(CCTexture2D*) getTexture:(int)nType order:(int)nOrder
{
    if (nType == 0)
    {
        switch (nOrder) 
        {
            case 1:
                return m_txtSolder1;
                break;
            case 2:
                return m_txtSolder2;
                break;
            case 3:
                return m_txtSolder3;
                break;
            default:
                break;
        }
    }
    if (nType == 1)
    {
        switch (nOrder) {
            case 1:
                return m_txtEnemy1;
                break;
            case 2:
                return m_txtEnemy2;
                break;
            case 3:
                return m_txtEnemy3;
                break;
            case 4:
                return m_txtEnemy4;
                break;
            case 5:
                return m_txtEnemy5;
                break;
            case 6:
                return m_txtEnemy6;
                break;
            default:
                break;
        }
    }
    return nil;
}


- (void) dealloc
{
	[super dealloc];
}



- (void)dismissHUD:(id)arg {
    
    [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
    self.hud = nil;
    
}
- (void)timeout:(id)arg {
    
    _hud.labelText = @"Timeout!";
    _hud.detailsLabelText = @"Please try again later.";
    _hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	_hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    
}

- (IBAction)retoreButtonTapped:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Purchase" message:@"Do you want to restore your previous in app purchase" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    alert.tag = 100;
    alert.delegate = self;
    [alert show];
    [alert release];
    self.hud = [MBProgressHUD showHUDAddedTo:[SettingsManager sharedManager].rootViewController.view animated:YES];
    _hud.labelText = @"Loading...";
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:30.0];
}


- (IBAction)removeAdsButtonTapped:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm" message:[NSString stringWithFormat:@"Do you want to remove ads for %@", [SettingsManager sharedManager].removeAdsValue] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    alert.tag = 200;
    alert.delegate = self;
    [alert show];
    [alert release];
    self.hud = [MBProgressHUD showHUDAddedTo:[SettingsManager sharedManager].rootViewController.view animated:YES];
    _hud.labelText = @"Loading...";
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
}

-(void)removeAds{
#ifdef FreeApp
    [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
    self.hud = [MBProgressHUD showHUDAddedTo:[SettingsManager sharedManager].rootViewController.view animated:YES];
    _hud.labelText = @"Purchasing...";
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
    [[MKStoreManager sharedManager] buyFeature:kInAppFeatureID
                                    onComplete:^(NSString* purchasedFeature,
                                                 NSData* purchasedReceipt,
                                                 NSArray* availableDownloads)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [SettingsManager sharedManager].hasInAppPurchaseBeenMade = YES;
         NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
         [standardUserDefaults setBool:[SettingsManager sharedManager].hasInAppPurchaseBeenMade forKey:@"inapp"];
         [standardUserDefaults synchronize];
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Purchase was successful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         [alert release];
         [self removeChildByTag:23 cleanup:YES];
         
         [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
     }
                                   onCancelled:^
     {
         NSLog(@"Something went wrong");
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Something went wrong.\nPlease try again in a moment" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         [alert release];
         [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
     }];
#endif
}

- (void)restore{
    [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
    self.hud = [MBProgressHUD showHUDAddedTo:[SettingsManager sharedManager].rootViewController.view animated:YES];
    _hud.labelText = @"Restoring Purchase...";
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
    
    [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^{
        [SettingsManager sharedManager].hasInAppPurchaseBeenMade = YES;
        
#ifdef FreeApp
        if([MKStoreManager isFeaturePurchased: kInAppFeatureID] == NO) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to process your transaction.\nPlease try again in a moment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Completed" message:@"In app Purchase Remove Ads Restored" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            [self removeChildByTag:23 cleanup:YES];
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setBool:[SettingsManager sharedManager].hasInAppPurchaseBeenMade forKey:@"inapp"];
            [standardUserDefaults synchronize];
        }
        [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
#endif
        
        
    } onError:^(NSError *A) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to process your transaction.\nPlease try again in a moment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [MBProgressHUD hideHUDForView:[SettingsManager sharedManager].rootViewController.view animated:YES];
    }];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        if (buttonIndex == 0) {
            [self restore];
        }else{
            [self dismissHUD:nil];
        }
    }else if (alertView.tag == 200){
        if (buttonIndex == 0) {
            [self removeAds];
        }else{
            [self dismissHUD:nil];
        }
        
    }
}

@end
