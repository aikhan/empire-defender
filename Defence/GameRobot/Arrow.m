//
//  Arrow.m
//  Defence
//
//  Created by huyingan on 12/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Arrow.h"
#import "GameViewLayer.h"
#import "Archer.h"
#import "Enemy.h"
#import "MainMenuLayer.h"

@implementation Arrow

@synthesize m_nOrderArcher;

-(id) init :(int)nOrder
{
	if( (self=[super init] )) {
        CCSprite *m_sCharacter = [CCSprite spriteWithFile:@"arrow.png"];
        m_sCharacter.position = ccp(0, 0);
        [self addChild:m_sCharacter z:LAYER_ENTITY];
        m_nOrderArcher = nOrder;
	}
	return self;
}

-(void) shooted
{
    [self schedule: @selector(updatePosition:) interval:0.01];
}

-(void) updatePosition:(ccTime) delta
{
    Archer *archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:m_nOrderArcher];
    
    int nRadius = nAryArrowSpeed[archer.m_nType];

    CGPoint pos = self.position;
    pos.x = pos.x - nRadius * cos(self.rotation * M_PI / 180);
    pos.y = pos.y + nRadius * sin(self.rotation * M_PI / 180);
    self.position = pos;
    
    // -------------- arrow's position --------------- //
    CGFloat x = archer.position.x + pos.x;
    CGFloat y = archer.position.y + pos.y;
    
    // -------------- if arrow is out of side -------------- //
    if ((x > g_tmxMap.contentSize.width * TILE_WIDTH) || (x < 0) || 
        (y > g_tmxMap.contentSize.height * TILE_HEIGHT) || (y < 0))
    {
        [self unscheduleAllSelectors];
        [self removeAllChildrenWithCleanup:YES];
        [[GameViewLayer ShareInstance].m_aryArrow removeObject:self];
        [self schedule: @selector(removeArrow:) interval:2];
    }
    
    // -------------- if arrow hit wall -------------- //
    CCSprite *m_sCharacter = [CCSprite spriteWithFile:@"arrow.png"];
    CGFloat fWidth = m_sCharacter.contentSize.width;
    CGFloat fHeight = m_sCharacter.contentSize.height;
    
    BOOL bCheck = NO;
    
    CGPoint ptPos = [self tilePosFromLocation:ccp(x - fWidth / 8, y - fHeight / 8)];
    int result = [self isBlock:ptPos];
    if (result > 0)
    {
        bCheck = YES;
    }
    ptPos = [self tilePosFromLocation:ccp(x - fWidth / 8, y + fHeight / 8)];
    result = [self isBlock:ptPos];
    if (result > 0)
    {
        bCheck = YES;
    }
    ptPos = [self tilePosFromLocation:ccp(x + fWidth / 8, y - fHeight / 8)];
    result = [self isBlock:ptPos];
    if (result > 0)
    {
        bCheck = YES;
    }
    ptPos = [self tilePosFromLocation:ccp(x + fWidth / 8, y + fHeight / 8)];
    result = [self isBlock:ptPos];
    if (result > 0)
    {
        bCheck = YES;
    }
    
    if (bCheck == YES)
    {
        if ((x < archer.position.x - OFFSET) || (x > archer.position.x + OFFSET))
        {
            [self hitMissed];
            return;
        }
    }

    // -------------- if arrow hit enemy -------------- //
    int nNumEnemy = [[GameViewLayer ShareInstance].m_aryEnemy count];
    for (int nIndex = 0; nIndex < nNumEnemy; nIndex ++)
    {
        Enemy *enemy = [[GameViewLayer ShareInstance].m_aryEnemy objectAtIndex:nIndex];
        
        if (enemy.m_bDie == YES)
            continue;
        
        if ((y - fHeight / 2 > enemy.position.y - 20) && (y - fHeight / 2 < enemy.position.y + 15) && 
            (x - fWidth / 2 > enemy.position.x - 20) && (x - fWidth / 2 < enemy.position.x + 20))
        {
            [self unscheduleAllSelectors];
            [self removeFromParentAndCleanup:YES];
            [enemy hittedArrow:self posX:(x - enemy.position.x + 20) posY:(y - enemy.position.y + 20)];
            [[GameViewLayer ShareInstance].m_aryArrow removeObject:self];
            [self schedule: @selector(removeArrow:) interval:2];

            
            [[MainMenuLayer ShareInstance] playEffectSound:@"enemy_hitted" nX:self.position.x nY:self.position.y];
            
            // ------------------------ check enemy's health ---------------------- //
            enemy.m_nHealth -= archer.m_nAttack;
            if (enemy.m_nHealth < 0)
            {
                [enemy removeAllChildrenWithCleanup:YES];
                [enemy unscheduleAllSelectors];
                [enemy stopAllActions];
                [enemy setAnimation:ACTION_DIE repeat:NO];
                enemy.m_bDie = YES;
                [enemy dieEnemy];
                
                return;
            }
        }
    }
}

-(void) hitMissed
{
    [[MainMenuLayer ShareInstance] playEffectSound:@"arrow_hit_miss" nX:self.position.x nY:self.position.y];

    Archer *archer = [[GameViewLayer ShareInstance].m_aryArcher objectAtIndex:m_nOrderArcher];

    [archer unschedule:@selector(shoot:)];
    [archer unschedule:@selector(selectAvailableEnemy:)];
    [archer unschedule:@selector(shootReady:)];

    archer.m_nSelEnemy = -1;
    [archer schedule: @selector(shootReady:) interval:ACTION_SPEED];
    
    [self unscheduleAllSelectors];
    [[GameViewLayer ShareInstance].m_aryArrow removeObject:self];
    [self schedule: @selector(removeArrow:) interval:2];
    
}

-(void) removeArrow:(ccTime) delta
{
    [self unscheduleAllSelectors];
    [self removeFromParentAndCleanup:YES];
}

-(int) isBlock: (CGPoint) tilePos
{
	int nIsBlock = 0;
    if(tilePos.x < g_tmxMap.mapSize.width && tilePos.x >= 0 && tilePos.y >= 0 && tilePos.y < g_tmxMap.mapSize.height)
    {
        int tileGID = [[g_tmxMap layerNamed:@"Stair_Layer"] tileGIDAt:tilePos];
        if(tileGID != 0)
        {
            NSDictionary *properties = [g_tmxMap propertiesForGID:tileGID];
            if(properties)
            {
                NSString *isBlock = [properties valueForKey:@"Stair"];
                nIsBlock = [isBlock intValue];
                isBlock = nil;
            }
        }
    }
	return nIsBlock;
}

-(CGPoint) tilePosFromLocation:(CGPoint) ptPos
{
	ptPos.x = (int)(ptPos.x / TILE_WIDTH);
	ptPos.y = (int)(ptPos.y / TILE_HEIGHT);
    ptPos.y = g_tmxMap.mapSize.height - ptPos.y - 1;
	return ptPos;
}

-(void) dealloc
{
	[super dealloc];
}

@end
