//
//  Archer.h
//  Defence
//
//  Created by huyingan on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstValue.h"
#import "Arrow.h"
#import "Soldier.h"

@interface Archer : Soldier {

    int         m_nCheck;
    Arrow       *m_arrow;
    BOOL        m_bShoot;
    CCSprite    *m_spDown;
    CCSprite    *m_spUp;
    int         m_nOrder;  // order index of array
    BOOL        m_bSeat;
    CCSprite    *m_spNoMove;
    CCSprite    *m_spSign;
    int         m_nDieCount;
}

-(id) init:(int)nType order:(int)nOrder;
-(void) attachImage;
-(void) attachArrow;
-(void) attachNoMove;
-(void) attachSign;
-(void) removeNoMove;
-(void) removeSign;
-(void) startAnimaion;
-(void) setArcherAnimation:(int)nType die:(BOOL)bDie;
-(void) startReadyAction;
-(void) dieArcher;
-(void) cleanupCharater;

@property (readwrite) int m_nOrder;
@property (readwrite) BOOL m_bSeat;
@property (retain, nonatomic) CCSprite    *m_spUp;

@end
