//
//  Archer.m
//  Defence
//
//  Created by huyingan on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Archer.h"
#import "GameViewLayer.h"
#import "Enemy.h"

@implementation Archer

@synthesize m_nOrder;
@synthesize m_bSeat;
@synthesize m_spUp;

-(id) init:(int)nType order:(int)nOrder
{
	if( (self=[super init] )) {
        
        m_spDown = [CCSprite spriteWithFile:[NSString stringWithFormat:@"soldier%d_down.png", nType + 1]];
        m_spUp = [CCSprite spriteWithFile:[NSString stringWithFormat:@"soldier%d_ready_1.png", nType + 1]];
        m_spDown.position = ccp(0, 0);
        m_spUp.position = ccp(0, 0);
        
        // -------- format values ----------- //
        m_nLevel = [GameViewLayer ShareInstance].m_nLevel;
        m_nType = nType;    // solider type : 3 ~ 7 (archer)
        m_bFightFlag = NO;
        m_bFlag = NO;
        m_nCheck = ACTION_NOTHING; // check running action
        m_bShoot = NO;
        m_bSeat = NO;
        m_bStop = NO;
        m_nOrder = nOrder;
        m_spNoMove = nil;
        m_nSelEnemy = -1;
        m_nDieCount = 100;
        m_bDie = NO;
        
        m_nHealth = nArySoldierHealth[nType];
        m_nAttack = nArySoldierAttack[nType];
        
        [self attachImage];
        [self attachArrow];
        [self addChild:m_spDown];
        [self addChild:m_spUp];
        [self attachNoMove];
        [self attachSign];
	}
	return self;
}

-(void) attachSign
{
    m_spSign = [CCSprite spriteWithFile:[NSString stringWithFormat:@"tooltip_bg%d.png", m_nType + 1]];
    m_spSign.position = ccp(70, 60);
    [self addChild:m_spSign z:LAYER_ENTITY];
}

-(void) removeSign
{
    if (m_spSign)
    {
        [self removeChild:m_spSign cleanup:YES];
    }
    m_spSign = nil;
}

-(void) attachNoMove
{
    if (!m_spNoMove)
    {
        m_spNoMove = [CCSprite spriteWithFile:@"no_move.png"];
        m_spNoMove.position = ccp(5, 0);
        [self addChild:m_spNoMove z:LAYER_ENTITY];
    }
}

-(void) removeNoMove
{
    if (m_spNoMove)
    {
        [self removeChild:m_spNoMove cleanup:YES];
    }
    m_spNoMove = nil;
}

-(void) attachImage
{
    [self setArcherAnimation:ACTION_READY die:NO];
}

-(void) attachArrow
{
    m_arrow = [[Arrow alloc] init:m_nOrder];
    m_arrow.position = ccp(0, 13);
    m_arrow.flipX = m_spUp.flipX;
    [self addChild:m_arrow z:LAYER_ENTITY];
    m_arrow.visible = NO;
    [[GameViewLayer ShareInstance].m_aryArrow addObject:m_arrow];
}

-(void) startAnimaion
{
    [self schedule: @selector(update:) interval:0.01];
}

-(void) setArcherAnimation:(int)nType die:(BOOL)bDie
{
    if (m_nCheck == nType) // if action is same , return
        return;
    
    [self stopAllActions];
    
    m_nCheck = nType;
    int nStep = 0;
    
    NSString *strName;
    
    switch (nType) {
        case ACTION_DIE:
            strName = @"die";
            nStep = 7;
            break;
        case ACTION_FIGHT1:
            strName = @"ready";
            nStep = 18;
            break;
        case ACTION_READY:
            strName = @"ready";
            nStep = 2;
            break;
        default:
            break;
    }
    
    CCAnimation *animation = [[CCAnimation alloc] initWithName:@"idle" delay:0.1];
    for(int i = 1; i < nStep; i++) {
        [animation addFrameWithFilename:[NSString stringWithFormat:@"soldier%d_%@_%i.png", m_nType + 1, strName, i]];
    }
    CCAnimate   *actionEffect = [CCAnimate actionWithAnimation:animation restoreOriginalFrame: NO];
    CCRepeat    *repeatAction = [CCRepeat actionWithAction:actionEffect times:1];
    
    strName = nil;
    
    if (bDie)
    {
        [m_spUp removeFromParentAndCleanup:YES];
        [m_spDown removeFromParentAndCleanup:YES];
        [self runAction: repeatAction];
    }
    else
    {
        [m_spUp runAction: repeatAction];
    }
}

-(void) readyActionEnd: (ccTime) delta
{
    [self setArcherAnimation:ACTION_READY die:NO];
}

-(void) update: (ccTime) delta
{

    [self moveCharacter];

    // -------------- ready action ----------- //
    if (m_bStop == YES)
    {
        
        [self unscheduleAllSelectors];
        [self startReadyAction];
    }
}

-(void) startReadyAction
{
    m_bShoot = NO;
    m_nCheck = ACTION_NOTHING;
    [self setArcherAnimation:ACTION_FIGHT1 die:NO];
    [self schedule: @selector(shootReady:) interval:ACTION_SPEED];
} 

-(void) shootReady: (ccTime) delta
{
    [self unschedule:@selector(shootReady:)];

    m_bFlag = NO;
    m_nSelEnemy = -1;
    [self schedule: @selector(selectAvailableEnemy:) interval:0.01];
}

-(void) shoot:(ccTime) delta
{
    m_bShoot = NO;

    [self unschedule:@selector(shoot:)];
    [self unschedule:@selector(selectAvailableEnemy:)];
    
    [m_arrow shooted];
    m_arrow.visible = YES;

    m_arrow = nil;

    [self attachArrow];
    [self startReadyAction];
}


-(void) dieArcher
{

    [self unscheduleAllSelectors];
    
    m_nSelEnemy = -1;
    
    m_bDie = YES;
    
    [self setArcherAnimation:ACTION_DIE die:YES];
    
    [self schedule: @selector(removeArcher:) interval:0.05];
}

-(void) removeArcher: (ccTime) delta
{
    m_nDieCount -= 5;
    
    [self setOpacity:m_nDieCount];
    
    if (m_nDieCount < 0)
    {
        [self unscheduleAllSelectors];
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
    }
}

-(void) selectAvailableEnemy: (ccTime) delta
{

    // -------------- select attack enemy -------------- //
    
    m_nSelEnemy = -1;
    m_nSelEnemy = [self selectEnemy];

    if ((m_nSelEnemy == -1) || ([[GameViewLayer ShareInstance].m_aryEnemy count] == 0))
        m_bFlag = NO;
    else
        m_bFlag = YES;
    
    if (m_nSelEnemy != -1)
    {
        
        Enemy *enemy = [[GameViewLayer ShareInstance].m_aryEnemy objectAtIndex:m_nSelEnemy];
        CGPoint pos = enemy.position;
        CGFloat angle = 0.0f;
        
        CGFloat width = self.position.x - pos.x;
        CGFloat height = self.position.y - pos.y + 10;
        
        angle = atan(height / width) * 180 / M_PI;
        if (pos.x > self.position.x)
        {
            m_spUp.flipX = 180;
            m_spDown.flipX = 180;
            m_arrow.flipX = 180;
            angle = 180 - angle;

            if (angle > 220.0f)
            {
                angle = 220.0f;
                return;
            }
            
            m_spUp.rotation = angle + 180;
        }
        else
        {
            m_spUp.flipX = 0;
            m_spDown.flipX = 0;
            m_arrow.flipX = 0;
            angle *= -1;

            if (angle < -40.0f)
            {
                angle = -40.0f;
                return;
            }
            
            m_spUp.rotation = angle;
        }
        
        
        m_arrow.rotation = angle;
        [self unschedule:@selector(selectAvailableEnemy:)];
        [self schedule:@selector(shoot:) interval:ACTION_SPEED];
    }
}

-(void) cleanupCharater {
    
    while ([self.children count] > 0) {
        [self removeChild:[self.children objectAtIndex:0] cleanup:YES];
    }
}

-(void) dealloc
{
    if (m_arrow)
    {
        [m_arrow removeFromParentAndCleanup:YES];
        m_arrow = nil;
    }
    [self cleanupCharater];
	[super dealloc];
}

@end
