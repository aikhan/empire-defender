//
//  CustomLabel.m
//  AREarth
//
//  Created by pirate on 11-8-2.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CustomLabel.h"


@implementation CustomLabel

@synthesize m_FontImg;

- (id)init:(CGFloat)width img_height:(CGFloat)height sepa_width:(int)sepa_number
{
    if (self == [super init])
    {
        m_fWidth = width;
        m_fHeight = height;
        m_nSepaNumber = sepa_number;
        
    }
    return self;
}

- (UIImage*)getPara_CustomFont:(UIImage *)imageToCrop nOrder:(int)order
{
    
    CGRect tempRect;
    CGFloat sepa_width = m_fWidth /(CGFloat) m_nSepaNumber;
    
    tempRect = CGRectMake(sepa_width * order, 0, sepa_width, m_fHeight);
    return [self imageByCropping:imageToCrop toRect:tempRect];
}

- (UIImage*)getCustomImage:(UIImage *)imageToCrop nOrder:(int)order
{
    CGRect tempRect;
    CGFloat sepa_width = m_fWidth /(CGFloat) m_nSepaNumber;
    
    tempRect = CGRectMake(0, 0, sepa_width * order, m_fHeight);
    return [self imageByCropping:imageToCrop toRect:tempRect];
}


- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)dealloc
{
    [super dealloc];
}
@end
