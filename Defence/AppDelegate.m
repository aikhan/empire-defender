//
//  AppDelegate.m
//  Defence
//
//  Created by huyingan on 1/2/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "GameConfig.h"
#import "HelloWorldLayer.h"
#import "RootViewController.h"
#import "SplashLayer.h"
#import "GameViewLayer.h"
#import "MainMenuLayer.h"

#import "SNAdsManager.h"
#import "SettingsManager.h"
#import "LocalNotificationManager.h"
#import "MKStoreManager.h"

@implementation AppDelegate

@synthesize window;
@synthesize currentLeaderBoard;
@synthesize currentScore;
@synthesize currentScoreLabel;
@synthesize gameCenterManager;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController

//	CC_ENABLE_DEFAULT_GL_STATES();
//	CCDirector *director = [CCDirector sharedDirector];
//	CGSize size = [director winSize];
//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
//	sprite.position = ccp(size.width/2, size.height/2);
//	sprite.rotation = -90;
//	[sprite visit];
//	[[director openGLView] swapBuffers];
//	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController	
}
- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	// Init the window
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	
	CCDirector *director = [CCDirector sharedDirector];
	
	// Init the View Controller
	viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	viewController.wantsFullScreenLayout = YES;
	
	//
	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
	EAGLView *glView = [EAGLView viewWithFrame:[window bounds]
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
	
	// attach the openglView to the director
	[director setOpenGLView:glView];
	
//	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
//	if( ! [director enableRetinaDisplay:YES] )
//		CCLOG(@"Retina Display Not supported");
	
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
	[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
#endif
	
	[director setAnimationInterval:1.0/60];
	//[director setDisplayFPS:YES];
	
	
	// make the OpenGLView a child of the view controller
	[viewController setView:glView];
	
	// make the View Controller a child of the main window
	//[window addSubview: viewController.view];
    window.rootViewController = viewController;
    [SettingsManager sharedManager].rootViewController = viewController;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [SettingsManager sharedManager].hasInAppPurchaseBeenMade = [standardUserDefaults boolForKey:@"inapp"];
#ifdef FreeApp
    [MKStoreManager sharedManager];
    if (![SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        [[SNAdsManager sharedManager] giveMeBootUpAd];
    }
#endif
#ifdef PaidApp
    [[SNAdsManager sharedManager] giveMePaidFullScreenAd];
#endif
	[window makeKeyAndVisible];
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	
	// Removes the startup flicker
	[self removeStartupFlicker];
	
	// Run the intro Scene
	[[CCDirector sharedDirector] runWithScene: [SplashLayer scene]];
    
    // init game center
    [self initGameCenter];
}


- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
    [self scheduleAlarm];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[CCDirector sharedDirector] startAnimation];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
#ifdef FreeApp
    if ([SettingsManager sharedManager].hasInAppPurchaseBeenMade) {
        return;
    }
    [[SNAdsManager sharedManager] giveMeWillEnterForegroundAd];
#endif
}

-(void) scheduleAlarm {
    LocalNotificationManager *localNotification = [[LocalNotificationManager alloc] initWithMessage:@"Defend your Kingdom before the enemies take over!"];
    
    [localNotification release];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
    NSLog(@"Recieved Notification %@",notif);
    
	application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
	CCDirector *director = [CCDirector sharedDirector];
	
	[[director openGLView] removeFromSuperview];
	
	[viewController release];
	
	[window release];
	
	[director end];	
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

-(void) initGameCenter {
	self.currentLeaderBoard = kLeaderboardID_NORMAL;
	self.currentScore = 0;
	
	if([GameCenterManager isGameCenterAvailable]) {		 
		self.gameCenterManager = [[[GameCenterManager alloc] init] autorelease];
		[self.gameCenterManager setDelegate:self];
		[self.gameCenterManager authenticateLocalUser];
		
	}else{
		
		// Thе current device dοеѕ nοt support Game Center.
		
	}
}

- (void) showLeaderboard
{
	if(gameCenterManager){
		GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
		if(leaderboardController != NULL)
		{
			leaderboardController.category = kLeaderboardID_NORMAL;
			leaderboardController.timeScope = GKLeaderboardTimeScopeToday;
			leaderboardController.leaderboardDelegate = self;
			[viewController presentModalViewController: leaderboardController animated: YES];
		}
	}
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController1
{
	[viewController1 dismissModalViewControllerAnimated: YES];
	[viewController1 release];
}

- (void) showAchievements
{
	if(gameCenterManager){
		GKAchievementViewController *achievements = [[GKAchievementViewController alloc] init];
		if(achievements != NULL)
		{
			achievements.achievementDelegate = self;
			[viewController presentModalViewController: achievements animated: YES];
		}
	}
}

- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController1;
{
	[viewController1 dismissModalViewControllerAnimated: YES];
	[viewController1 release];
}

- (void) increaseScore:(long) nScore mode:(int) nMode
{
	if(gameCenterManager){
        if (nMode == 0)
            self.currentLeaderBoard = kLeaderboardID_NORMAL;
        if (nMode == 1)
            self.currentLeaderBoard = kLeaderboardID_SURVIVAL;
        
        self.currentScore = nScore;
        //[self checkAchievements];
        [self submitScore];
	}
}
- (void) checkAchievements
{
	NSString* identifier = NULL;
	double percentComplete = 0;
	identifier= kAchievement20Taps;
	if(identifier!= NULL)
	{
		[self.gameCenterManager submitAchievement: identifier percentComplete: percentComplete];
	}
}

- (void) reset
{
    /*
     if(gameCenterManager){
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Arе уου sure уου want tο reset уουr score аnd achievements?"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:@"Reset"
     otherButtonTitles:nil];
     [actionSheet showInView:[viewController view]];
     [actionSheet release];
     }
     */
}

- (void) submitScore
{
    [self.gameCenterManager reportScore: self.currentScore forCategory: self.currentLeaderBoard];
}

-(long) getHighScoreForCategory {
	if(gameCenterManager) {
		return [gameCenterManager reloadHighScoresForCategory:self.currentLeaderBoard];
	}
	return 0;
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex == 0) {
		
		[gameCenterManager resetAchievements];
		
		self.currentScore = 0;
		currentScoreLabel.text = [NSString stringWithFormat: @"%ld", self.currentScore];
		
	}
}

- (void) achievementSubmitted: (GKAchievement*) ach error:(NSError*) error;
{
	
	if((error == NULL) && (ach != NULL))
	{
		if(ach.percentComplete == 100.0) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Achievement Earned!"
															message:[NSString stringWithFormat:@"%@", ach.identifier]
														   delegate:nil
												  cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
		}
		
	}
	else
	{
		// Achievement Submission Failed.
		
	}
	
}


- (void)dealloc {
    [gameCenterManager release];
    [currentLeaderBoard release];
    [currentScoreLabel release];
	self.gameCenterManager = nil;
    self.currentLeaderBoard = nil;
    self.currentScoreLabel = nil;

    
	[[CCDirector sharedDirector] end];
	[window release];
	[super dealloc];
}

@end
