//
//  MainMenuLayer.h
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <AVFoundation/AVAudioPlayer.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface MainMenuLayer : CCLayer <AVAudioPlayerDelegate, UIAlertViewDelegate>{

    CCSprite        *backSprite;

    CCMenu          *mainMenu;
    CCMenuItem      *item_new;
    CCMenuItem      *item_load;
    CCMenuItem      *item_setting;
    CCMenuItem      *item_help;
    CCMenuItem      *item_highscore;
    CCMenuItem      *item_normal;
    CCMenuItem      *item_survival;
    CCMenuItem      *item_back;

    CGFloat         m_fMenuStart_X;
    CGFloat         m_fMenu_X;
    CGFloat         m_fNormal_X;
    CGFloat         m_fSurvival_X;
    CGFloat         m_fLoad_X;
    CGFloat         m_fSetting_X;
    CGFloat         m_fHighscore_X;
    CGFloat         m_fHelp_X;
    
    BOOL            m_bStop;
    
    AVAudioPlayer   *back_player;
    NSString        *path;
    NSError         *error;

    AppDelegate     *appDelegate;
    
    
    CCTexture2D     *m_txtSolder1;
    CCTexture2D     *m_txtSolder2;
    CCTexture2D     *m_txtSolder3;
    CCTexture2D     *m_txtEnemy1;
    CCTexture2D     *m_txtEnemy2;
    CCTexture2D     *m_txtEnemy3;
    CCTexture2D     *m_txtEnemy4;
    CCTexture2D     *m_txtEnemy5;
    CCTexture2D     *m_txtEnemy6;

    CCSprite        *m_spLoading;

}

+(id) scene;
+(MainMenuLayer*) ShareInstance;
-(void) attachMenu;
-(void) clickNewGame: (id) sender;
-(void) clickLoadGame: (id) sender;
-(void) clickSetting: (id) sender;
-(void) clickHighScore: (id) sender;
//-(void) clickHelp: (id) sender;
-(void) clickNormal: (id) sender;
-(void) clickSurvival: (id) sender;
-(void) clickBack: (id) sender;
- (void)playBackSound:(NSString*)filename;
- (void)playEffectSound:(NSString*)filename nX:(int)nX nY:(int)nY;
- (void)removeBackSound;
- (void)resumeBackSound;
- (void)pauseBackSound;
-(CCTexture2D*) getTexture:(int)nType order:(int)nOrder;
-(void) loadingAnimation;

@property (nonatomic, retain) CCTexture2D *m_txtSolder1;
@property (nonatomic, retain) CCTexture2D *m_txtSolder2;
@property (nonatomic, retain) CCTexture2D *m_txtSolder3;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy1;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy2;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy3;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy4;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy5;
@property (nonatomic, retain) CCTexture2D *m_txtEnemy6;
@property (retain) MBProgressHUD *hud;
@end
