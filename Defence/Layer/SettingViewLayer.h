//
//  MainMenuLayer.h
//  Defence
//
//  Created by huyingan on 12/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface SettingViewLayer : CCLayer
{

    CCSprite        *backSprite;
    CCSprite        *backMusic;
    CCSprite        *effectMusic;

    CCMenu          *mainMenu;
    CCMenuItem      *item_back;
    CCMenuItem      *item_bg_on;
    CCMenuItem      *item_bg_off;
    CCMenuItem      *item_effect_on;
    CCMenuItem      *item_effect_off;

    BOOL            m_bBackMusic;
    BOOL            m_bEffectMusic;
}

+(id) scene;
+(SettingViewLayer*) ShareInstance;
-(void) attachMenu;

-(void) clickBack: (id) sender;
-(void) clickBackMusic: (id) sender;
-(void) clickEffectMusic: (id) sender;
-(void) refreshCheckbox;

@property (readwrite) BOOL m_bBackMusic;
@property (readwrite) BOOL m_bEffectMusic;

@end
