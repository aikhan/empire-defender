//
//  SoldierModel.h
//  Defence
//
//  Created by huyingan on 12/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ConstValue.h"

@interface SoldierModel : CCLayer {
    
    CCSprite    *m_spCharacter;
    int         m_nType;
    int         m_nMoney;
}

-(id) init:(int)type money:(int)nMoney;
-(void) checkCharacter:(int)nMoney;
-(void) cleanupCharater;

@end
