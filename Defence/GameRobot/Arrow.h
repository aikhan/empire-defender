//
//  Arrow.h
//  Defence
//
//  Created by huyingan on 12/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Entity.h"
#import "ConstValue.h"


@interface Arrow : Entity {

    int         m_nOrderArcher;

}

-(id) init :(int)nOrder;
-(void) shooted;
-(int) isBlock: (CGPoint) tilePos;
-(void) hitMissed;
-(CGPoint) tilePosFromLocation:(CGPoint) ptPos;

@property (readwrite) int m_nOrderArcher;
@end
