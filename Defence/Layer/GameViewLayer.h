//
//  GameViewLayer.h
//  Defence
//
//  Created by huyingan on 12/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Enemy.h"
#import "CustomLabel.h"
#import "SoldierModel.h"
#import "Archer.h"
#import "Fighter.h"
#import "AppDelegate.h"

@interface GameViewLayer : CCLayer
{

    AppDelegate     *appDelegate;
    CCSprite        *backSprite;
    CCSprite        *skySprite;
    CCSprite        *underSkySprite;
    int             m_nLevel;
    int             m_nGold;
    int             m_nTime;
    int             m_nScore;
    Enemy           *m_enemy;
    NSMutableArray  *m_aryEnemy;
    NSMutableArray  *m_aryFighter;
    NSMutableArray  *m_aryArcher;
    NSMutableArray  *m_aryArrow;
    Archer          *m_archer;
    Fighter         *m_fighter;
    
    CGFloat         m_fDeviceScale;
    CGFloat         m_fFirstPinch;
    BOOL            m_bSound;
    int             m_nFlag;
    int             m_nMoney;
    
    CCMenu          *menu;
    CCMenuItem      *item_music;
    CCMenuItem      *item_music_disable;
    CCMenuItem      *item_repeat;
    CCMenuItem      *item_pause;
    CCMenuItem      *item_resume;
    CCMenuItem      *item_exit;
    
    CCSprite        *m_iconMedal;
    CCSprite        *m_iconTimer;
    
    CCSprite        *m_spLevel;
    CCSprite        *m_spGold;
    CCSprite        *m_spMoney;
    CCSprite        *m_spScore;
    CCSprite        *m_spTime;
    CCSprite        *m_spJewel;
    CCLayerColor    *overLayer;

    BOOL            m_bPause;
    BOOL            m_bExit;
    SoldierModel    *m_SModel[8];
    
    BOOL            m_bCreate;

    
//---------------------- Message Board ---------------- //
    
    CCSprite        *spBack;
    CCSprite        *spMsg;
    CCMenu          *msg_menu;
    CCMenuItem      *item_yes;
    CCMenuItem      *item_no;
}

+(id) scene;
+(GameViewLayer*) ShareInstance;
+(int) getRandomNumberBetweenMin:(int)min andMax:(int)max;
+(void) setInfo:(int)nLevel money:(int)nMoney score:(int)nScore;

-(void) attachEnvironment;
-(void) createSoldier:(int)nType left:(int)nX top:(int)nY;
-(void) createEnemy;
-(void) createButton;

-(void) clickMusic: (id) sender;
-(void) clickMusicDisable: (id) sender;
-(void) clickRepeat: (id) sender;
-(void) clickPause: (id) sender;
-(void) clickExit: (id) sender;
-(void) clickExitYes: (id) sender;
-(void) clickExitNo: (id) sender;
-(void) clickRepeatYes: (id) sender;
-(void) clickPlayNext: (id) sender;


-(CCSprite*) attachInfo:(int)nValue type:(int)nType xPos:(int)nX yPos:(int)nY;
-(void) attachJewel:(int)nValue type:(int)nType xPos:(int)nX yPos:(int)nY;
-(CCSprite*) getCharacterSprite:(int)value type:(int)nType;

-(void) reshowCharacter;
-(void) pauseGame;
-(void) showMessage:(int)nType;
-(void) closeMessage;
-(void) removeAllContents;
-(void) updateLevelNumber;
-(void) updateGoldNumber;
-(void) updateMoneyNumber:(int)nMoney;
-(void) updateScoreNumber:(int)nScore;
-(void) updateTimeNumber;
-(void) cleanup;


@property (readwrite) int m_nLevel;
@property (nonatomic, retain) Archer *m_archer;
@property (nonatomic, retain) Fighter *m_fighter;
@property (nonatomic, retain) NSMutableArray  *m_aryEnemy;
@property (nonatomic, retain) NSMutableArray  *m_aryFighter;
@property (nonatomic, retain) NSMutableArray  *m_aryArcher;
@property (nonatomic, retain) NSMutableArray  *m_aryArrow;
@property (readwrite) int m_nGold;
@property (readwrite) int m_nMoney;
@property (readwrite) BOOL m_bCreate;

@end
