//
//  SNManager.h
//  Alien Tower
//
//  Created by Asad Khan on 05/02/2013.
//
//

#import <Foundation/Foundation.h>

#import <Availability.h>

/*
#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif
*/

#ifdef DEBUG
#define DebugLog(f, ...) NSLog(f, ## __VA_ARGS__)
#else
#define DebugLog(f, ...)
//#define NS_BLOCK_ASSERTIONS
#endif




#define kPlayHavenAdTimeOutThresholdValue 4.0
#define kRevMobAdTimeOutThresholdValue 3.0

#ifdef FreeApp
#define kRevMobId @"516e6cc3b6e71af85d00005c"

#define ChartBoostAppID @"516e67a217ba479e2b000005"
#define ChartBoostAppSignature @"e8d820eadec48f0f59739bf1bdf5122f7c27bb83"

#define MOBCLIX_ID @"B7C698A4-FFF5-4B2F-822F-25170F8858C3"
#define kPlayHavenAppToken @"6a77b63e22834ce2a4f2e629c0fbd8cd"
#define kPlayHavenSecret @"05d5218d9061461691a63700a88f7bb6"
#define kPlayHavenPlacement @"main_menu"

#define kTapJoyAppID @""
#define kTapJoySecretKey @""
#define kRateURL @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=634210668"

#endif

#ifdef PaidApp
#define kRevMobId @"516e6cc3b6e71af85d00005c"

#define ChartBoostAppID @"516e67a217ba479e2b000005"
#define ChartBoostAppSignature @"e8d820eadec48f0f59739bf1bdf5122f7c27bb83"

#define MOBCLIX_ID @""
#define kPlayHavenAppToken @""
#define kPlayHavenSecret @""
#define kPlayHavenPlacement @"main_menu"

#define kTapJoyAppID @""
#define kTapJoySecretKey @""
#define kRateURL @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=634211483"
#endif

typedef NS_ENUM(NSUInteger, adPriorityLevel){
    kPriorityLOWEST = 10,
    kPriorityNORMAL,
    kPriorityHIGHEST
};

typedef NS_ENUM(NSUInteger, ConnectionStatus) {
   
    kNotAvailable,
    kWANAvailable,
    kWifiAvailable
    
};

/*
 These are the default values before changing them do consult Angela
 */
#define kRevMobBannerAdPriority kPriorityHIGHEST  //In Game banner Ads
#define kRevMobFullScreenAdPriority kPriorityLOWEST //Full Screen Pop-ups
#define kRevMobButtonAdPriority kPriorityHIGHEST //Button ads this is not currently used in games, its just a wrapper on Link Ads
#define kRevMobLinkAdPriority kPriorityHIGHEST  //This is the Ad that is displayed on buttons on game over screens
#define kRevMobPopAdPriority kPriorityHIGHEST  //UIAlert type pop-up Ads in games
#define kRevMobLocalNotificationAdPriority kPriorityHIGHEST // UILocalNotification Ads //Currently we're not using it

#define kChartBoostFullScreeAdPriority kPriorityHIGHEST
#define kChartBoostMoreAppsAd kPriorityHIGHEST

#define kMobClixBannerAdPriority kPriorityNORMAL
#define kMobClixFullScreenAdPriority kPriorityNORMAL


#define kPlayHavenFullScreenAdPriority kPriorityNORMAL



#define kNumberOfAdNetworks 5





@interface SNManager : NSObject



@end
